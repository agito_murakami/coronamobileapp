package com.example.projectmantap11.models;

import java.util.List;


public class ReimbursementServerResponse {

    private ErrorSchema error_schema;
    private OutputSchemaReimbursementList output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public OutputSchemaReimbursementList getOutput_schema() {
        return output_schema;
    }

    //LIST CLASS
    public class OutputSchemaReimbursementList {
        private Reimbursements reimburses;

        public Reimbursements getReimburses() {
            return reimburses;
        }
    }


    //INNER LIST CLASS
    public class Reimbursements {

        private List<Reimbursement> content;
        private Pageable pageable;

        private boolean last;
        private int total_pages;
        private int total_elements;

        private Sort sort;
        private int number_of_elements;
        private boolean first;
        private int size;
        private int number;
        private boolean empty;

        public List<Reimbursement> getContent() {
            return content;
        }

        public void setContent(List<Reimbursement> content) {
            this.content = content;
        }

        public Pageable getPageable() {
            return pageable;
        }

        public void setPageable(Pageable pageable) {
            this.pageable = pageable;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getTotal_elements() {
            return total_elements;
        }

        public void setTotal_elements(int total_elements) {
            this.total_elements = total_elements;
        }

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public int getNumber_of_elements() {
            return number_of_elements;
        }

        public void setNumber_of_elements(int number_of_elements) {
            this.number_of_elements = number_of_elements;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }
    }
}
