package com.example.projectmantap11.models;

public class CompanyServerResponse {
    private ErrorSchema error_schema;
    private OutputSchemaCompany output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaCompany getOutput_schema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaCompany output_schema) {
        this.output_schema = output_schema;
    }

    public class OutputSchemaCompany {
        private Company company;

        public Company getCompany() {
            return company;
        }

        public void setCompany(Company company) {
            this.company = company;
        }
    }
}
