package com.example.projectmantap11.models;

import java.util.List;

public class VCListServerResponse {

    public ErrorSchema error_schema;
    public OutputSchema output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchema getOutput_schema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchema output_schema) {
        this.output_schema = output_schema;
    }

    public class OutputSchema {
        private VirtualCards virtual_cards;

        public VirtualCards getVirtual_cards() {
            return virtual_cards;
        }

        public void setVirtual_cards(VirtualCards virtual_cards) {
            this.virtual_cards = virtual_cards;
        }
    }

    public class VirtualCards {
        private List<VirtualCard> content;
        private Pageable pageable;
        private boolean last;
        private int total_pages;
        private int total_elements;
        private Sort sort;
        private int number_of_elements;
        private boolean first;
        private int size;
        private int number;
        private boolean empty;

        public List<VirtualCard> getContent() {
            return content;
        }

        public void setContent(List<VirtualCard> content) {
            this.content = content;
        }

        public Pageable getPageable() {
            return pageable;
        }

        public void setPageable(Pageable pageable) {
            this.pageable = pageable;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getTotal_elements() {
            return total_elements;
        }

        public void setTotal_elements(int total_elements) {
            this.total_elements = total_elements;
        }

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public int getNumber_of_elements() {
            return number_of_elements;
        }

        public void setNumber_of_elements(int number_of_elements) {
            this.number_of_elements = number_of_elements;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }
    }
}
