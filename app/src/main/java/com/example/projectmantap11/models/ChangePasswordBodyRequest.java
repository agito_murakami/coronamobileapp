package com.example.projectmantap11.models;

public class ChangePasswordBodyRequest {
    String id;
    String old_password;
    String new_password;

    public ChangePasswordBodyRequest(String id, String oldPassword, String newPassword) {
        this.id = id;
        this.old_password = oldPassword;
        this.new_password = newPassword;
    }
}
