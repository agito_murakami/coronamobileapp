package com.example.projectmantap11.models;

public class KaryawanServerResponse {
    private ErrorSchema error_schema;
    private OutputSchemaKaryawan output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaKaryawan getOutput_schema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaKaryawan output_schema) {
        this.output_schema = output_schema;
    }

    public class OutputSchemaKaryawan {
        private Karyawan user;

        public Karyawan getUser() {
            return user;
        }

        public void setUser(Karyawan user) {
            this.user = user;
        }
    }

}
