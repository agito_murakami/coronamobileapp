package com.example.projectmantap11.models;

import java.util.List;

public class VCRequestListServerResponse {

    private ErrorSchema error_schema;
    private OutputSchemaVCRList output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaVCRList getOutput_schema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaVCRList output_schema) {
        this.output_schema = output_schema;
    }

    public class OutputSchemaVCRList{
        private VirtualCardRequests virtual_card_requests;

        public VirtualCardRequests getVirtual_card_requests() {
            return virtual_card_requests;
        }

        public void setVirtual_card_requests(VirtualCardRequests virtual_card_requests) {
            this.virtual_card_requests = virtual_card_requests;
        }
    }

    public class VirtualCardRequests {
        private List<VirtualCard> content;
        private Pageable pageable;
        private int total_pages;
        private int total_elements;
        private boolean last;
        private Sort sort;
        private int number_of_elements;
        private boolean first;
        private int size;
        private int number;
        private boolean empty;

        public List<VirtualCard> getContent() {
            return content;
        }

        public void setContent(List<VirtualCard> content) {
            this.content = content;
        }

        public Pageable getPageable() {
            return pageable;
        }

        public void setPageable(Pageable pageable) {
            this.pageable = pageable;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getTotal_elements() {
            return total_elements;
        }

        public void setTotal_elements(int total_elements) {
            this.total_elements = total_elements;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public int getNumber_of_elements() {
            return number_of_elements;
        }

        public void setNumber_of_elements(int number_of_elements) {
            this.number_of_elements = number_of_elements;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }
    }

}
