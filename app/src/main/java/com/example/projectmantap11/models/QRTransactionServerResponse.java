package com.example.projectmantap11.models;

public class QRTransactionServerResponse {
    private ErrorSchema error_schema;
    private OutputSchemaQRTransaction output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaQRTransaction getOutputSchema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaQRTransaction output_schema) {
        this.output_schema = output_schema;
    }


    //outputschema class
    public class OutputSchemaQRTransaction {
        private QRTransaction transaction;

        public QRTransaction getTransaction() {
            return transaction;
        }
    }
}

