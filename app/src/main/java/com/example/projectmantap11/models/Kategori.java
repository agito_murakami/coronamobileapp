package com.example.projectmantap11.models;

public class Kategori {
    /*
     "uuid": "cat02",
      "company_id": "c001",
      "name": "Makan",
      "limit_transaction": 10000000,
      "flag_reimburse": true,
      "flag_virtual_card": true,
      "created_date": "2020-06-17T00:00:00.000+0700",
      "modify_date": "2020-06-17T00:00:00.000+0700"
     */

    private String uuid;
    private String company_id;
    private String name;
    private int limit_transaction;
    private boolean flag_reimburse;
    private boolean flag_virtual_card;
    private String created_date;
    private String modify_date;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLimit_transaction() {
        return limit_transaction;
    }

    public void setLimit_transaction(int limit_transaction) {
        this.limit_transaction = limit_transaction;
    }

    public boolean isFlag_reimburse() {
        return flag_reimburse;
    }

    public void setFlag_reimburse(boolean flag_reimburse) {
        this.flag_reimburse = flag_reimburse;
    }

    public boolean isFlag_virtual_card() {
        return flag_virtual_card;
    }

    public void setFlag_virtual_card(boolean flag_virtual_card) {
        this.flag_virtual_card = flag_virtual_card;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }
}
