package com.example.projectmantap11.models;

import java.util.List;

public class KategoriServerResponse {

    private ErrorSchema error_schema;
    private OutputSchemaKategoriList output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaKategoriList getOutputSchema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaKategoriList output_schema) {
        this.output_schema = output_schema;
    }

    public class OutputSchemaKategoriList {
        private Categories categories;


        public Categories getCategories() {
            return categories;
        }
    }

    public class Categories {
        private List<Kategori> content;
        private Pageable pageable;
        private int total_pages;
        private int total_elements;
        private boolean last;
        private Sort sort;
        private int number_of_elements;
        private boolean first;
        private int size;
        private int number;
        private boolean empty;

        public List<Kategori> getContent() {
            return content;
        }

        public Pageable getPageable() {
            return pageable;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public int getTotal_elements() {
            return total_elements;
        }

        public boolean isLast() {
            return last;
        }

        public Sort getSort() {
            return sort;
        }

        public int getNumber_of_elements() {
            return number_of_elements;
        }

        public boolean isFirst() {
            return first;
        }

        public int getSize() {
            return size;
        }

        public int getNumber() {
            return number;
        }

        public boolean isEmpty() {
            return empty;
        }
    }
}
