package com.example.projectmantap11.models;

public class QRTransactionSend {

    private String card_number;
    private String card_date;
    private String pin;
    private String merchant;
    private String account_number;
    private double amount;
    private String coordinate_start;
    private String coordinate_end;
    private boolean coordinate_valid;

    //constructor

    public QRTransactionSend(
            String card_number
            , String card_date
            , String pin
            , String merchant
            , String account_number
            , double amount
            , String coordinate_start
            , String coordinate_end
            , boolean is_coordinate_valid) {

        this.card_number = card_number;
        this.card_date = card_date;
        this.pin = pin;
        this.merchant = merchant;
        this.account_number = account_number;
        this.amount = amount;
        this.coordinate_start = coordinate_start;
        this.coordinate_end = coordinate_end;
        this.coordinate_valid = is_coordinate_valid;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_date() {
        return card_date;
    }

    public void setCard_date(String card_date) {
        this.card_date = card_date;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCoordinate_start() {
        return coordinate_start;
    }

    public void setCoordinate_start(String coordinate_start) {
        this.coordinate_start = coordinate_start;
    }

    public String getCoordinate_end() {
        return coordinate_end;
    }

    public void setCoordinate_end(String coordinate_end) {
        this.coordinate_end = coordinate_end;
    }

    public boolean isIs_coordinate_valid() {
        return coordinate_valid;
    }

    public void setIs_coordinate_valid(boolean is_coordinate_valid) {
        this.coordinate_valid = is_coordinate_valid;
    }
}
