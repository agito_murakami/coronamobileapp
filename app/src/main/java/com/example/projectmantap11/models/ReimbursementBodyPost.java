package com.example.projectmantap11.models;

public class ReimbursementBodyPost {

    private String category;
    private Float amount;
    private String description;
    private String file_path;
    private String coordinate_start;
    private String coordinate_end;
    private boolean coordinate_valid;

    public ReimbursementBodyPost(String category, Float amount, String description, String file_path,
                                 String coordinate_start, String coordinate_end, boolean coordinate_valid ) {
        this.category = category;
        this.amount = amount;
        this.description = description;
        this.file_path = file_path;
        this.coordinate_start = coordinate_start;
        this.coordinate_end = coordinate_end;
        this.coordinate_valid = coordinate_valid;
    }
}
