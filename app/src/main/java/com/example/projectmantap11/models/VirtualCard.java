package com.example.projectmantap11.models;

public class VirtualCard {

    private String uuid;
    private String company_id;
    private String branch_uuid;
    private String division_uuid;
    private String user_uuid;
    private String invoice;
    private String card_number;
    private String limit_transaction;
    private String limit_used;
    private String limit_each_transaction;
    private String start_date;
    private String end_date;
    private String status;
    private Boolean is_active;
    private String created_date;
    private String modify_date;
    private String name;
    private String categories;

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getCategory() {
        return categories;
    }

    public void setCategory(String category) {
        this.categories = category;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getBranch_uuid() {
        return branch_uuid;
    }

    public void setBranch_uuid(String branch_uuid) {
        this.branch_uuid = branch_uuid;
    }

    public String getDivision_uuid() {
        return division_uuid;
    }

    public void setDivision_uuid(String division_uuid) {
        this.division_uuid = division_uuid;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getInvoice_request() {
        return invoice;
    }

    public void setInvoice_request(String invoice_request) {
        this.invoice = invoice_request;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getLimit_transaction() {
        return limit_transaction;
    }

    public void setLimit_transaction(String limit_transaction) {
        this.limit_transaction = limit_transaction;
    }

    public String getLimit_used() {
        return limit_used;
    }

    public void setLimit_used(String limit_used) {
        this.limit_used = limit_used;
    }

    public String getLimit_each_transaction() {
        return limit_each_transaction;
    }

    public void setLimit_each_transaction(String limit_each_transaction) {
        this.limit_each_transaction = limit_each_transaction;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
