package com.example.projectmantap11.models;

public class NewVCRBodyRequest {
    private String category;
    private String start_date;
    private String end_date;
    private String amount;
    private String description;

    public NewVCRBodyRequest(String category, String start_date, String end_date, String amount, String description) {
        this.category = category;
        this.start_date = start_date;
        this.end_date = end_date;
        this.amount = amount;
        this.description = description;
    }
}
