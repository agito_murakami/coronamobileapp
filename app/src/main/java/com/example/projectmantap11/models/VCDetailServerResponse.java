package com.example.projectmantap11.models;

import com.example.projectmantap11.activity.Transaction;

import java.util.List;

public class VCDetailServerResponse {
    private ErrorSchema error_schema;
    private OutputSchemaVCDetail output_schema;

    public ErrorSchema getError_schema() {
        return error_schema;
    }

    public void setError_schema(ErrorSchema error_schema) {
        this.error_schema = error_schema;
    }

    public OutputSchemaVCDetail getOutput_schema() {
        return output_schema;
    }

    public void setOutput_schema(OutputSchemaVCDetail output_schema) {
        this.output_schema = output_schema;
    }


    //INNER CLASSES
    public class OutputSchemaVCDetail{
        private List<Report> reports;
        private List<Transaction> transactions;
        private VirtualCard virtual_card;
        private Karyawan user;

        public List<Report> getReports() {
            return reports;
        }

        public void setReports(List<Report> reports) {
            this.reports = reports;
        }

        public List<Transaction> getTransactions() {
            return transactions;
        }

        public void setTransactions(List<Transaction> transactions) {
            this.transactions = transactions;
        }

        public VirtualCard getVirtual_card() {
            return virtual_card;
        }

        public void setVirtual_card(VirtualCard virtual_card) {
            this.virtual_card = virtual_card;
        }

        public Karyawan getUser() {
            return user;
        }

        public void setUser(Karyawan user) {
            this.user = user;
        }
    }
}
