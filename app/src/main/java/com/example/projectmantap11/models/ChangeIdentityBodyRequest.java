package com.example.projectmantap11.models;

public class ChangeIdentityBodyRequest {

    private String uuid;
    private String email;
    private String phone;
    private String address;

    public ChangeIdentityBodyRequest(String uuid, String email, String phone, String address) {
        this.uuid = uuid;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }
}