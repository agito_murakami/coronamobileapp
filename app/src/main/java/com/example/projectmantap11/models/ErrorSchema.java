package com.example.projectmantap11.models;

public class ErrorSchema {
    private String error_code;
    private ErrorMessage error_message;

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public ErrorMessage getError_message() {
        return error_message;
    }

    public void setError_message(ErrorMessage error_message) {
        this.error_message = error_message;
    }
}