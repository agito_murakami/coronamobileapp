package com.example.projectmantap11.models;

public class ActivateVirtualCardBodyRequest {

    private String uuid;
    private String user_uuid;
    private String card_number;
    private String old_pin;
    private String new_pin;

    public ActivateVirtualCardBodyRequest(String uuid, String user_uuid, String card_number, String old_pin, String new_pin) {
        this.uuid = uuid;
        this.user_uuid = user_uuid;
        this.card_number = card_number;
        this.old_pin = old_pin;
        this.new_pin = new_pin;
    }

}
