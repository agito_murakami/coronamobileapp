package com.example.projectmantap11.services;

import com.example.projectmantap11.models.ActivateVirtualCardBodyRequest;
import com.example.projectmantap11.models.Authorization;
import com.example.projectmantap11.models.ChangeIdentityBodyRequest;
import com.example.projectmantap11.models.ChangePasswordBodyRequest;
import com.example.projectmantap11.models.CompanyServerResponse;
import com.example.projectmantap11.models.GetOCRPriceBodyRequest;
import com.example.projectmantap11.models.Karyawan;
import com.example.projectmantap11.models.KaryawanServerResponse;
import com.example.projectmantap11.models.KategoriServerResponse;
import com.example.projectmantap11.models.NewVCRBodyRequest;
import com.example.projectmantap11.models.NotificationServerResponse;
import com.example.projectmantap11.models.QRTransactionSend;
import com.example.projectmantap11.models.QRTransactionServerResponse;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.ReimbursementBodyPost;
import com.example.projectmantap11.models.ReimbursementServerResponse;
import com.example.projectmantap11.models.VCDetailServerResponse;
import com.example.projectmantap11.models.VCListServerResponse;
import com.example.projectmantap11.models.VCRequestListServerResponse;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    //authorization
    @Multipart
    @POST("oauth/token")
    Call<Authorization> loginAccount(@Header("Authorization") String authkey,
                                     @Part("grant_type") RequestBody granttype,
                                     @Part("username") RequestBody username,
                                     @Part("password") RequestBody password);

    //GET corp data
    //use AUTH
    @GET("api/my-company")
    Call<CompanyServerResponse> getCompanyData(@Header("Authorization") String authkey);

    //karyawan
    @GET("api/daftar-karyawan")
    Call<List<Karyawan>> getDaftarAllKaryawan(@Header("Authorization") String authkey);

    //get karyawan detail
    //use auth
    @GET("api/user/{uuid}")
    Call<KaryawanServerResponse> getKaryawanDetail (@Header("Authorization") String authkey,
                                                    @Path("uuid") String uuid);

    //reimbursement*****************************************************************************
    @GET("/api/my-reimburse")
    Call<ReimbursementServerResponse> getDaftarAllReimbursement(@Header("Authorization") String authkey,
                                                                @Query("progress") int progress,
                                                                @Query("page") int page,
                                                                @Query("category") String kategori,
                                                                @Query("status") String status,
                                                                @Query("order_by") String order,
                                                                @Query("direction") String direction);

    @POST("/api/reimburse")
    Call<Reimbursement> addNewReimbursement(@Header("Authorization") String authkey,
                                            @Body ReimbursementBodyPost body);

    //virtualcard*******************************************************************************

    //virtualcardrequest
    //get new virtual card
    @POST("api/vcr")
    Call<ResponseBody> postNewVCR(@Header("Authorization") String authkey,
                                  @Body NewVCRBodyRequest Body);


    //virtualcardactive list
    //resource-transactionCORONA.apps.pcf.dti.co.id/
    //use this to get all active virtual card
    @GET("api/employee/virtual-card")
    Call<VCListServerResponse> getDaftarVC(@Header("Authorization") String authkey);

    //virtualcardactive list
    //resource-transactionCORONA.apps.pcf.dti.co.id/
    //use this to get all active virtual card
    @GET("api/employee/virtual-card")
    Call<VCListServerResponse> getDaftarActiveVC(@Header("Authorization") String authkey,
                                                 @Query("active") Boolean active);

    //GET virtual card detail ***NOT DONE***
    //resource-transactioncorona.apps.pcf.dti.co.id/
    //use this to get specific detail on a virtual card
    @GET("api/employee/virtual-card/{id}")
    Call<VCDetailServerResponse> getVirtualCardDetail(@Header("Authorization") String authkey,
                                                      @Path("id")String id);

    //GET virtual card request list
    //resource-transactioncorona.apps.pcf.dti.co.id/
    //use this to get lists of request list virtual card
    @GET("api/my-vcr")
    Call<VCRequestListServerResponse> getRequestVirtualCard(@Header("Authorization") String authkey,
                                                            @Query("page") int page,
                                                            @Query("category") String kategori,
                                                            @Query("status") String status,
                                                            @Query("order_by") String order,
                                                            @Query("direction") String direction);

    //GET category
    @GET("api/category")
    Call<KategoriServerResponse> getDaftarKategori(@Header("Authorization") String authkey,
                                                   @Query("page") int page,
                                                   @Query("type") String type);



    //transaction
    @POST("api/transaction-qr")
    Call<QRTransactionServerResponse> bayarPakeQR(@Header("Authorization") String authkey,
                                                  @Body QRTransactionSend body);

    //ACTIVATE VIRTUAL CARD
    //USE TRANSACTION URL
    @PUT("api/employee/virtual-card/activation")
    Call<ResponseBody> putActivateVirtualCard(@Header("Authorization") String authkey,
                                              @Body ActivateVirtualCardBodyRequest body);

    //change password
    @PUT("api/ubah-password")
    Call<ResponseBody> changepassword(@Header("Authorization") String authkey,
                                      @Body ChangePasswordBodyRequest body);

    //change identity
    @PUT("api/change-identity")
    Call<ResponseBody> putChangeIdentity(@Header("Authorization") String authkey,
                                      @Body ChangeIdentityBodyRequest body);

    //OCR
    //http://corona-ocr.apps.pcf.dti.co.id/ocr
    @POST("ocr")
    Call<ResponseBody> postOCRHarga(@Body GetOCRPriceBodyRequest link);

    //notification
    ///api/notification/${user_uuid}?admin=1
    //https://resource-notifCORONA.apps.pcf.dti.co.id
    @GET("api/notification/{useruuid}")
    Call<NotificationServerResponse> getNotification(@Header("Authorization") String authkey,
                                                     @Path("useruuid")String userUuid,
                                                     @Query("admin")int admin);
}