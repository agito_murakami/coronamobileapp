package com.example.projectmantap11.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.projectmantap11.services.RetrofitClientInstance.getUnsafeOkHttpClient;

public class RetrofitNotificationClientInstance {
    private static final String BASE_RESO_URL = "https://resource-notifCORONA.apps.pcf.dti.co.id/";

    private static Retrofit retrofit;


    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_RESO_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
        }
        return retrofit;
    }
}
