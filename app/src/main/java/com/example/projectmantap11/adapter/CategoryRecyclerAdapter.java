package com.example.projectmantap11.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Kategori;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int onPosition = 0;

    private List<Kategori> dataList = new ArrayList<>();
    private final CategoryRecyclerAdapter.ListItemClickListener<Kategori> mOnclickListener;

    //reference for parent
    RecyclerView mRecyclerView;

    public interface ListItemClickListener<T> {
        void onListItemClicked(Kategori t);
    }

    public CategoryRecyclerAdapter(Context context, CategoryRecyclerAdapter.ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<Kategori> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_kategori, parent, false);
            mRecyclerView = (RecyclerView) parent;
            return new CategoryRecyclerAdapter.MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            mRecyclerView = (RecyclerView) parent;
            return new CategoryRecyclerAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CategoryRecyclerAdapter.MenuViewHolder) {
            populateItemRows((CategoryRecyclerAdapter.MenuViewHolder) holder, position);
        } else if (holder instanceof CategoryRecyclerAdapter.LoadingViewHolder) {
            showLoadingView((CategoryRecyclerAdapter.LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private View chooser;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;

            title = (TextView) itemView.findViewById(R.id.recycler_item_kategori);
            chooser = (View) itemView.findViewById(R.id.recycler_item_katogori_choice);
        }

        void bind(final Kategori employee) {
            if (onPosition == -1) {
                chooser.setBackgroundResource(R.drawable.background_textbox);
            } else {
                if (onPosition == getAdapterPosition()) {
                    chooser.setBackgroundResource(R.drawable.background_statustext_blue);
                } else {
                    chooser.setBackgroundResource(R.drawable.background_textbox);
                }
            }
            title.setText(employee.getName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chooser.setBackgroundResource(R.drawable.background_statustext_blue);
                    if (onPosition != getAdapterPosition()) {
                        notifyItemChanged(onPosition);
                        onPosition = getAdapterPosition();
                    }
                }
            });
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(CategoryRecyclerAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(CategoryRecyclerAdapter.MenuViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    public String getSelected() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getUuid();
        }
        return null;
    }

    public int getMaxHarga() {
        if(onPosition != -1) {
            return dataList.get(onPosition).getLimit_transaction();
        } else {
            return 0;
        }
    }

    public String getCategoryName() {
        if(onPosition != -1) {
            return dataList.get(onPosition).getName();
        } else {
            return null;
        }
    }

}
