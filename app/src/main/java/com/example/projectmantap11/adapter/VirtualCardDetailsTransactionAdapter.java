package com.example.projectmantap11.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.activity.Transaction;
import com.example.projectmantap11.activity.VirtualCardDetailsActivity;
import com.example.projectmantap11.activity.VirtualCardListActivity;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.VirtualCard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;

public class VirtualCardDetailsTransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int onPosition = 0;

    private List<Transaction> dataList = new ArrayList<>();
    private final VirtualCardDetailsTransactionAdapter.ListItemClickListener<Transaction> mOnclickListener;

    public interface ListItemClickListener<T> {
        void onListItemClicked(VirtualCard t);
    }

    public VirtualCardDetailsTransactionAdapter(Context context
            , VirtualCardDetailsTransactionAdapter.ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<Transaction> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_vc_transaction, parent, false);
            return new VirtualCardDetailsTransactionAdapter.MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            return new VirtualCardDetailsTransactionAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof VirtualCardDetailsTransactionAdapter.MenuViewHolder) {
            populateItemRows((VirtualCardDetailsTransactionAdapter.MenuViewHolder) holder, position);
        } else if (holder instanceof VirtualCardDetailsTransactionAdapter.LoadingViewHolder) {
            showLoadingView((VirtualCardDetailsTransactionAdapter.LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView transactionMerchant;
        private TextView transactionUUID;
        private TextView transactionAmount;
        private TextView transactionDate;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;
            transactionAmount = itemView.findViewById(R.id.recycler_item_vcdetails_transaction_amount);
            transactionUUID = itemView.findViewById(R.id.recycler_item_vcdetails_transaction_id);
            transactionMerchant = itemView.findViewById(R.id.recycler_item_vcdetails_transaction_merchant_name);
            transactionDate = itemView.findViewById(R.id.recycler_item_vcdetails_transaction_date);
        }

        //this is your onclick view generator
        void bind(final Transaction employee) {
            if (onPosition == -1) {

            } else {
                if (onPosition == getAdapterPosition()) {

                } else {

                }
            }

            //SETTEXT
            double amount = employee.getAmount();
            Locale indo = new Locale("id", "ID");
            NumberFormat indoFormat = NumberFormat.getCurrencyInstance(indo);
            transactionAmount.setText(indoFormat.format(amount));

            transactionUUID.setText(employee.getUuid());
            transactionMerchant.setText(employee.getMerchant());

            String transactionDateString = employee.getCreated_date();
            String[] transactionDateFormatList = transactionDateString.split("T");
            String[] transactionDateTimeFormatList = transactionDateFormatList[1].split(":");
            String transactionDateInput = transactionDateFormatList[0] +
                    ", " + transactionDateTimeFormatList[0] +
                    ":" + transactionDateTimeFormatList[1];
            transactionDate.setText(transactionDateInput);


            //ONCLICK LISTENER
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (onPosition != getAdapterPosition()) {
                        notifyItemChanged(onPosition);
                        onPosition = getAdapterPosition();
                    }
                }
            });
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(VirtualCardDetailsTransactionAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(VirtualCardDetailsTransactionAdapter.MenuViewHolder holder, int position) {

        holder.bind(dataList.get(position));
    }

    public String getTransanctionUUID() {
        if(onPosition!= -1) {
            return dataList.get(onPosition).getUuid();
        }
        return null;
    }

    public String getMerchant() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getMerchant();
        }
        return null;
    }

    public double getAmountPaid() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getAmount();
        }
        return 0;
    }

}
