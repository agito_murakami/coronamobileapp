package com.example.projectmantap11.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.VirtualCard;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class VirtualCardRequestRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<VirtualCard> dataList = new ArrayList<>();
    private final VirtualCardRequestRecyclerAdapter.ListItemClickListener<VirtualCard> mOnclickListener;

    public interface ListItemClickListener<T> {
        void onListItemClicked(VirtualCard t);
    }

    public VirtualCardRequestRecyclerAdapter(Context context, VirtualCardRequestRecyclerAdapter.ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<VirtualCard> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_reimburse, parent, false);
            return new VirtualCardRequestRecyclerAdapter.MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            return new VirtualCardRequestRecyclerAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof VirtualCardRequestRecyclerAdapter.MenuViewHolder) {
            populateItemRows((VirtualCardRequestRecyclerAdapter.MenuViewHolder) holder, position);
        } else if (holder instanceof VirtualCardRequestRecyclerAdapter.LoadingViewHolder) {
            showLoadingView((VirtualCardRequestRecyclerAdapter.LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView dates;
        private TextView duit;
        private TextView status;
        private FrameLayout statusBlock;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;
            duit = (TextView) itemView.findViewById(R.id.reimburse_value);
            title = (TextView) itemView.findViewById(R.id.reimburse_title);
            dates = (TextView) itemView.findViewById(R.id.reimburse_date);
            status = (TextView) itemView.findViewById(R.id.reimburse_status);
            statusBlock = (FrameLayout) itemView.findViewById(R.id.status_text_block);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(VirtualCardRequestRecyclerAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(VirtualCardRequestRecyclerAdapter.MenuViewHolder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnclickListener.onListItemClicked(dataList.get(position));
            }
        });
        holder.title.setId(position);
        final int i = holder.title.getId();
        holder.title.setText(dataList.get(position).getInvoice_request());

        String[] arrOfStr = dataList.get(position).getCreated_date().split("T");
        holder.dates.setText(arrOfStr[0]);

        holder.duit.setText(dataList.get(position).getCategory());
        holder.status.setText(dataList.get(position).getStatus());

        if (dataList.get(position).getStatus().equals("ACCEPTED")) {
            holder.status.setText("Accepted");
            holder.statusBlock.setBackgroundResource(R.drawable.background_statustext_green);
        } else if (dataList.get(position).getStatus().equals("PENDING")) {
            holder.status.setText("Pending");
            holder.statusBlock.setBackgroundResource(R.drawable.background_statustext_yellow);
        } else if (dataList.get(position).getStatus().equals("REJECTED")) {
            holder.status.setText("Rejected");
            holder.statusBlock.setBackgroundResource(R.drawable.background_statustext_red);
        } else {
            holder.status.setText("Submitted");
            holder.statusBlock.setBackgroundResource(R.drawable.background_statustext_blue);
        }
    }

}
