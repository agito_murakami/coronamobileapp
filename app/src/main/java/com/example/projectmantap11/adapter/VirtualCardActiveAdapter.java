package com.example.projectmantap11.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.VirtualCard;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;

public class VirtualCardActiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int onPosition = 0;

    private List<VirtualCard> dataList = new ArrayList<>();
    private final VirtualCardActiveAdapter.ListItemClickListener<VirtualCard> mOnclickListener;

    public interface ListItemClickListener<T> {
        void onListItemClicked(VirtualCard t);
    }

    public VirtualCardActiveAdapter(Context context, VirtualCardActiveAdapter.ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<VirtualCard> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_choose_active_virtual_card, parent, false);
            return new VirtualCardActiveAdapter.MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            return new VirtualCardActiveAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof VirtualCardActiveAdapter.MenuViewHolder) {
            populateItemRows((VirtualCardActiveAdapter.MenuViewHolder) holder, position);
        } else if (holder instanceof VirtualCardActiveAdapter.LoadingViewHolder) {
            showLoadingView((VirtualCardActiveAdapter.LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView cardCategory;
        private TextView cardNumber;
        private FrameLayout statusBlock;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;
            cardNumber = (TextView) itemView.findViewById(R.id.card_number);
            cardCategory = (TextView) itemView.findViewById(R.id.card_category);
            statusBlock = (FrameLayout) itemView.findViewById(R.id.chosen_text_block);
        }

        void bind(final VirtualCard employee) {
            if (onPosition == -1) {
                statusBlock.setBackgroundResource(R.drawable.background_textbox);
            } else {
                if (onPosition == getAdapterPosition()) {
                    statusBlock.setBackgroundResource(R.drawable.background_statustext_blue);
                } else {
                    statusBlock.setBackgroundResource(R.drawable.background_textbox);
                }
            }

            //SETTEXT
            double amount = Double.parseDouble(employee.getLimit_transaction()) -
                    Double.parseDouble(employee.getLimit_used());

            String amountString = "Rp " + String.valueOf(amount);

            cardNumber.setText(employee.getCard_number());
            cardCategory.setText(amountString);

            //ONCLICK LISTENER
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    statusBlock.setBackgroundResource(R.drawable.background_statustext_blue);
                    if (onPosition != getAdapterPosition()) {
                        notifyItemChanged(onPosition);
                        onPosition = getAdapterPosition();
                    }
                }
            });
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(VirtualCardActiveAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(VirtualCardActiveAdapter.MenuViewHolder holder, int position) {

        holder.bind(dataList.get(position));
    }

    public String getCardNumberSelected() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getCard_number();
        }
        return null;
    }

    public String getCardDateSelected() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getEnd_date();
        }
        return null;
    }

}
