package com.example.projectmantap11.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Notification;
import com.example.projectmantap11.models.Reimbursement;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<Notification> dataList = new ArrayList<>();
    private final ListItemClickListener<Notification> mOnclickListener;

    public interface ListItemClickListener<T> {
        void onListItemClicked(Notification t);
    }

    public NotificationRecyclerAdapter(Context context, ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<Notification> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_notification, parent, false);
            return new MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof  MenuViewHolder) {
            populateItemRows((MenuViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView dates;
        private TextView message;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;
            title = (TextView) itemView.findViewById(R.id.notification_item_title);
            dates = (TextView) itemView.findViewById(R.id.notification_item_date);
            message = (TextView) itemView.findViewById(R.id.notification_item_description);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(MenuViewHolder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnclickListener.onListItemClicked(dataList.get(position));
            }
        });
        holder.title.setId(position);
        final int i = holder.title.getId();
        holder.title.setText(dataList.get(position).getUuid());

        String[] arrOfDay = dataList.get(position).getCreated_date().split("T");
        String[] arrTime = arrOfDay[1].split("\\.");
        holder.dates.setText(arrOfDay[1]);

        holder.message.setText(dataList.get(position).getMessage());

        if (dataList.get(position).isIs_read()) {
            holder.title.setTypeface(Typeface.DEFAULT_BOLD);
            holder.message.setTypeface(Typeface.DEFAULT_BOLD);
            holder.dates.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

}
