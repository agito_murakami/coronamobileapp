package com.example.projectmantap11.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.activity.VirtualCardDetailsActivity;
import com.example.projectmantap11.activity.VirtualCardListActivity;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.VirtualCard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;

public class VirtualCardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int onPosition = 0;

    private List<VirtualCard> dataList = new ArrayList<>();
    private final VirtualCardListAdapter.ListItemClickListener<VirtualCard> mOnclickListener;

    public interface ListItemClickListener<T> {
        void onListItemClicked(VirtualCard t);
    }

    public VirtualCardListAdapter(Context context, VirtualCardListAdapter.ListItemClickListener onClickListener) {
        this.context = context;
        this.mOnclickListener = onClickListener;
    }

    public void setMenuList (List<VirtualCard> menuList) {
        dataList = new ArrayList<>();
        dataList = menuList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_virtual_card, parent, false);
            return new VirtualCardListAdapter.MenuViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recycler_item_loading, parent, false);
            return new VirtualCardListAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof VirtualCardListAdapter.MenuViewHolder) {
            populateItemRows((VirtualCardListAdapter.MenuViewHolder) holder, position);
        } else if (holder instanceof VirtualCardListAdapter.LoadingViewHolder) {
            showLoadingView((VirtualCardListAdapter.LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        private TextView cardNumber;
        private TextView cardStatus;
        private TextView cardEndDate;
        private TextView cardAmountRemaining;
        private ImageView cardIcon;
        private View itemView;

        public MenuViewHolder(View itemView) {
            super(itemView);
            this.itemView=itemView;
            cardNumber = (TextView) itemView.findViewById(R.id.recycler_item_virtual_card_card_number);
            cardStatus = itemView.findViewById(R.id.recycler_item_virtual_card_status);
            cardIcon = itemView.findViewById(R.id.recycler_item_virtual_card_icon);
            cardEndDate = itemView.findViewById(R.id.recycler_item_virtual_card_end_date);
            cardAmountRemaining = itemView.findViewById(R.id.recycler_item_virtual_card_amount_remaining);
        }

        //this is your onclick view generator
        void bind(final VirtualCard employee) {
            if (onPosition == -1) {

            } else {
                if (onPosition == getAdapterPosition()) {

                } else {

                }
            }

            //SETTEXT
            double amount = Double.parseDouble(employee.getLimit_transaction()) -
                    Double.parseDouble(employee.getLimit_used());

            Locale indo = new Locale("id", "ID");
            NumberFormat indoFormat = NumberFormat.getCurrencyInstance(indo);

            cardNumber.setText(employee.getCard_number());
            cardAmountRemaining.setText(indoFormat.format(amount));
            cardStatus.setText(employee.getStatus());

            //set color
            if (employee.getStatus().equals("ACTIVED")) {
                cardStatus.setTextColor(Color.GREEN);
                cardIcon.setColorFilter(Color.GREEN);
            } else if (employee.getStatus().equals("RELEASED")) {
                cardStatus.setTextColor(Color.BLUE);
                cardIcon.setColorFilter(Color.BLUE);
            }

            String endDate = employee.getEnd_date();
            String[] endDateFormatted = endDate.split("T");
            cardEndDate.setText(endDateFormatted[0]);


            //ONCLICK LISTENER
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (onPosition != getAdapterPosition()) {
                        notifyItemChanged(onPosition);
                        onPosition = getAdapterPosition();
                    }

                    Intent i  = new Intent(view.getContext(), VirtualCardDetailsActivity.class);
                    i.putExtra("vcuuid", getCardUUID());
                    view.getContext().startActivity(i);
                }
            });
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(VirtualCardListAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(VirtualCardListAdapter.MenuViewHolder holder, int position) {

        holder.bind(dataList.get(position));
    }

    public String getCardUUID() {
        if(onPosition!= -1) {
            return dataList.get(onPosition).getUuid();
        }
        return null;
    }

    public String getCardNumberSelected() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getCard_number();
        }
        return null;
    }

    public String getCardDateSelected() {
        if (onPosition != -1) {
            return dataList.get(onPosition).getEnd_date();
        }
        return null;
    }

}
