package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;

import com.example.projectmantap11.R;
import com.example.projectmantap11.services.internalservices.OnClearFromRecentService;

public class SplashScreenActivity extends AppCompatActivity {

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash_screen);

        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
//                /* Create an Intent that will start the Menu-Activity. */
//                Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
//                SplashScreenActivity.this.startActivity(mainIntent);
//                SplashScreenActivity.this.finish();

                //TAKE BOOLEAN OF FIRST WELCOME
                SharedPreferences prefs = getSharedPreferences("CORONA_PROJECT_MANTAP_WELCOME", Context.MODE_PRIVATE);
                boolean welcomeactivity = prefs.getBoolean("welcomeactivity", false);
                Log.d("WELCOMEACTIVITY", "YOUR WELCOME ACTIVITY DONE: " + welcomeactivity);

                if (welcomeactivity) {

                    Intent intent = new Intent();
                    intent.setClass(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent();
                    intent.setClass(SplashScreenActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                }


                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

        getSupportActionBar().hide();
    }
}