package com.example.projectmantap11.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.projectmantap11.R;

public class SetPINActivity extends AppCompatActivity {

    private Button setPinSubmitButton;
    private PinView setPinMake, setPinConfirm;

    //GLOBAL SHAREPREFSETUP
    private SharedPreferences sharedPrefsetup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_p_i_n);

        //HIDE SUPPORT ACTION BAR
        getSupportActionBar().hide();

        //GET KEYS FOR PIN AND FP
        sharedPrefsetup = getSharedPreferences("CORONA_SETUP", Context.MODE_PRIVATE);
        boolean isPinSet = sharedPrefsetup.getBoolean("setpin", false);
        boolean isUsingFP = sharedPrefsetup.getBoolean("setFP", false);

        //FIND VIEW
        setPinSubmitButton = findViewById(R.id.set_pin_confirmation_button);
        setPinMake = findViewById(R.id.setpinmakeview);
        setPinConfirm = findViewById(R.id.setpinconfirmview);

        //ONCLICK
        setPinSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String make = setPinMake.getText().toString();
                String confirm = setPinConfirm.getText().toString();

                Toast.makeText(SetPINActivity.this, "make : " + make.length()
                        + ", confirm : " + confirm.length(), Toast.LENGTH_SHORT).show();

                if (make.length() != 6 || confirm.length() != 6) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SetPINActivity.this);
                    builder.setCancelable(true);
                    builder.setTitle("PIN must be 6 characters, please check your inputs!");
                    builder.setNegativeButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if (make.equals(confirm)) { //SET SHAREDPREF
                        //SHAREDPREF COMMIT
                        SharedPreferences.Editor editor = sharedPrefsetup.edit();
                        editor.putBoolean("setpin", true);
                        editor.putString("loginpin", make);
                        editor.commit();

                        if (isUsingFP) {
                            Intent skip = new Intent(SetPINActivity.this, FingerprintActivity.class);
                            startActivity(skip);
                            finish();
                        } else {
                            useFPDialog();
                        }

                    } else { //SHOW DIALOG CONFIRM PIN
                        AlertDialog.Builder builder = new AlertDialog.Builder(SetPINActivity.this);
                        builder.setCancelable(true);
                        builder.setTitle("PIN must be be same for both forms, please check your inputs!");
                        builder.setNegativeButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            }
        });

    }

    private void useFPDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SetPINActivity.this);
        builder.setCancelable(true);
        builder.setTitle("Do you want to use fingerprint for login?");
        builder.setMessage("Please select : ");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //SHAREDPREF COMMIT
                        SharedPreferences.Editor editor = sharedPrefsetup.edit();
                        editor.putBoolean("setFP", true);
                        editor.commit();

                        Intent skip = new Intent(SetPINActivity.this, FingerprintActivity.class);
                        startActivity(skip);
                        finish();
                    }
                });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent skip = new Intent(SetPINActivity.this, FingerprintActivity.class);
                startActivity(skip);
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}