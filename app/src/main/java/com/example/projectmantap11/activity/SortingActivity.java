package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.CategoryRecyclerAdapter;
import com.example.projectmantap11.adapter.StatusRecyclerAdapter;
import com.example.projectmantap11.helper.InputFilterMinMax;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.KategoriServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitResoClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SortingActivity extends AppCompatActivity
        implements StatusRecyclerAdapter.ListItemClickListener , CategoryRecyclerAdapter.ListItemClickListener{

    private RecyclerView categoryFilterRV, statusFilterRV, orderBySortRV, directionSortRV;

    private String key;

    //list for status RV
    private StatusRecyclerAdapter statusRecyclerAdapter;
    private List<String> statusList = new ArrayList<String>();

    //list for status RV
    private StatusRecyclerAdapter orderRecyclerAdapter;
    private List<String> orderList = new ArrayList<String>();

    //list for status RV
    private StatusRecyclerAdapter directionRecyclerAdapter;
    private List<String> directionList = new ArrayList<String>();

     //list for RV
    private List<Kategori> kategoriList = new ArrayList<Kategori>();
    private CategoryRecyclerAdapter categoryRecyclerAdapter;

    //pagination for category
    private int pageCategory = 0;
    private int maxPageCategory = 0;
    int maxItemCategory = 0;
    boolean isLoading = false;

    //Progress dialog
    private  ProgressDialog progressDoalog;

    //button confirm
    private Button confirmButtonSorting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sorting);

        //hide actionbar
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEY", "loadInitial: " + key);

        //get intent to choose between reimbursement and virtual card
        Intent intent = getIntent();
        boolean isReimbursement = intent.getBooleanExtra("isreimbursement", true);

        //findviews
        categoryFilterRV = findViewById(R.id.recycler_sorting_category_reimbursement);
        statusFilterRV = findViewById(R.id.recycler_sorting_status_reimbursement);
        orderBySortRV = findViewById(R.id.recycler_sorting_sort_order);
        directionSortRV = findViewById(R.id.recycler_sorting_sort_direction);
        confirmButtonSorting = findViewById(R.id.confirm_sorting_button);

        //setup statusFilterRV
        statusFilterRV.setLayoutManager(new GridLayoutManager(this, 2));
        statusFilterRV.setHasFixedSize(true);
        statusRecyclerAdapter = new StatusRecyclerAdapter(this, this);
        statusList.add("SUBMITTED");
        statusList.add("REJECTED");
        statusList.add("PENDING");
        statusList.add("ACCEPTED");
        statusRecyclerAdapter.setMenuList(statusList);
        statusFilterRV.setAdapter(statusRecyclerAdapter);

        //setup orderBySortRV
        orderBySortRV.setLayoutManager(new GridLayoutManager(this, 2));
        orderBySortRV.setHasFixedSize(true);
        orderRecyclerAdapter = new StatusRecyclerAdapter(this, this);
        orderList.add("Created Date");
        orderList.add("Amount");
        orderList.add("Modify Date");
        orderRecyclerAdapter.setMenuList(orderList);
        orderBySortRV.setAdapter(orderRecyclerAdapter);

        //setup directionSortRV
        directionSortRV.setLayoutManager(new GridLayoutManager(this, 2));
        directionSortRV.setHasFixedSize(true);
        directionRecyclerAdapter = new StatusRecyclerAdapter(this, this);
        directionList.add("Ascending");
        directionList.add("Descending");
        directionRecyclerAdapter.setMenuList(directionList);
        directionSortRV.setAdapter(directionRecyclerAdapter);

        //setup category RV
        categoryFilterRV.setLayoutManager(new LinearLayoutManager(this));
        categoryFilterRV.setHasFixedSize(true);
        categoryRecyclerAdapter = new CategoryRecyclerAdapter(this, this);
        categoryRecyclerAdapter.setMenuList(kategoriList);
        categoryFilterRV.setAdapter(categoryRecyclerAdapter);
        if (isReimbursement) {
            callEnqueKategoriAPI("reimburse");
        } else {
            callEnqueKategoriAPI("virtualcard");
        }


        //confirm sorting button on click
        confirmButtonSorting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String statusInput = statusRecyclerAdapter.getSelected();
                String categoryInput = categoryRecyclerAdapter.getCategoryName();

                String orderResult = orderRecyclerAdapter.getSelected();
                String orderListResult[] = orderResult.split(" ");
                String orderInput = orderListResult[0];
                for (int i = 1;i < orderListResult.length;i++) {
                    orderInput = orderInput + "_" + orderListResult[i];
                }
                orderInput = orderInput.toLowerCase();

                String directionResult = directionRecyclerAdapter.getSelected();
                String directionInput = "";
                if(directionResult.equals("Ascending")) {
                    directionInput = "asc";
                } else {
                    directionInput = "desc";
                }


                Log.d("SORTING", "onClick: Status : " + statusInput);
                Log.d("SORTING", "onClick: Category : " + categoryInput);
                Log.d("SORTING", "onClick: Order : " + orderInput);
                Log.d("SORTING", "onClick: Direction : " + directionInput);

                Intent data = new Intent();
                data.putExtra("categoryfilter", categoryInput);
                data.putExtra("statusfilter", statusInput);
                data.putExtra("orderfilter", orderInput);
                data.putExtra("directionfilter", directionInput);
                // Activity finished ok, return the data
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private void callEnqueKategoriAPI(String x) {

        progressDoalog = new ProgressDialog(SortingActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getDaftarKategori("Bearer " + key, pageCategory, x)
                .enqueue(new Callback<KategoriServerResponse>() {
                    @Override
                    public void onResponse(Call<KategoriServerResponse> call, Response<KategoriServerResponse> response) {
                        //Log.d("getrawkategori", "onResponse: " + response.body());

                        progressDoalog.dismiss();

                        if (response.body() != null) {

                            try {
                                Log.d("get raw", "getraw: " + response.raw());
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getOutputSchema().getCategories().getContent());
                                kategoriList.addAll(response.body().getOutputSchema().getCategories().getContent());
                                categoryRecyclerAdapter.notifyDataSetChanged();

                                maxPageCategory = response.body().getOutputSchema().getCategories().getTotal_pages();
                                maxItemCategory = response.body().getOutputSchema().getCategories().getTotal_elements();

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("ARDA Kategori"
                                        , "onResponse: DATA FAILURE" +
                                                e);
                                Toast.makeText(SortingActivity.this, "" + e
                                        , Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<KategoriServerResponse> call, Throwable t) {
                        Log.d("ARDA Kategori", "onFailure: " + t);
                        Toast.makeText(SortingActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onListItemClicked(Kategori t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}