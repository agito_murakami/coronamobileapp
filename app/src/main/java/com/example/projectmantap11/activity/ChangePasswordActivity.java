package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.ChangePasswordBodyRequest;
import com.example.projectmantap11.models.ReimbursementServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.core.view.Change;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputLayout oldPassInputLayout, newPassInputLayout, confirmPassInputLayout;
    TextInputEditText oldPassEditText, newPassEditText, confirmPassEditText;

    Button changePassHitAPIButton;

    //your key here
    String key = "your key here";
    String uuid = "your uuid here";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //hideactionbar
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYCHANGEPASS", "loadInitial: " + key);

        //take uuid
        uuid = prefs.getString("uuid", "empty");
        Log.d("UUIDCHANGEPASS", "onCreate: " + uuid);

        //findview
        oldPassInputLayout = findViewById(R.id.change_password_old_password_input_layout);
        newPassInputLayout = findViewById(R.id.change_password_new_password_input_layout);
        confirmPassInputLayout = findViewById(R.id.change_password_confirm_password_input_layout);
        changePassHitAPIButton = findViewById(R.id.confirm_change_password);

        oldPassEditText = findViewById(R.id.change_password_old_password_input);
        newPassEditText = findViewById(R.id.change_password_new_password_input);
        confirmPassEditText = findViewById(R.id.change_password_confirm_password_input);

        oldPassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!oldPassEditText.getText().toString().equals("teststaff")) {
                    oldPassInputLayout.setError("Old password is wrong");
                    if (oldPassEditText.getText().toString()
                            .equals(newPassEditText.getText().toString())) {
                        newPassInputLayout.setError("New password is same as old password");
                    } else {
                        newPassInputLayout.setError(null);
                    }
                } else {
                    oldPassInputLayout.setError(null);
                }
            }
        });

        newPassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String comparison = oldPassEditText.getText().toString();
                if (newPassEditText.getText().toString().equals(comparison)) {
                    newPassInputLayout.setError("New password is same as old password");
                } else {
                    newPassInputLayout.setError(null);
                }
            }
        });

        confirmPassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String comparison = newPassEditText.getText().toString();
                if (!confirmPassEditText.getText().toString().equals(comparison)) {
                    confirmPassEditText.setError("Password not match!");
                } else {
                    confirmPassInputLayout.setError(null);
                }
            }
        });

        changePassHitAPIButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEnqueAPI();
            }
        });
    }

    private void callEnqueAPI() {

        ChangePasswordBodyRequest input = new ChangePasswordBodyRequest(uuid, oldPassEditText.getText().toString()
                ,newPassEditText.getText().toString());

        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(ChangePasswordActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitClientInstance
                .getRetrofitInstance()
                .create(ApiService.class)
                .changepassword("Bearer " + key, input)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        progressDoalog.dismiss();
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());
                        if (response.isSuccessful()) {
                            // Do awesome stuff
                            Toast.makeText(ChangePasswordActivity.this,
                                    "Password Changed Successfully ",
                                    Toast.LENGTH_SHORT).show();

                            Intent i = getBaseContext().getPackageManager()
                                    .getLaunchIntentForPackage(getBaseContext().getPackageName() );
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                        } else if (response.code() == 401) {
                            // Handle unauthorized
                            Toast.makeText(ChangePasswordActivity.this,
                                    "Unauthorized access! Your password hasn't been changed",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Handle other responses

//                            String rererere = new Gson().toJson(response);
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String rere = jObjError.get("message").toString();

                                Toast.makeText(ChangePasswordActivity.this, rere, Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
//                                Toast.makeText(ChangePasswordActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            Toast.makeText(ChangePasswordActivity.this,
                                    "Unknown error! Your password hasn't been changed : "
                                    ,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        progressDoalog.dismiss();
                        Log.d("REIMBURSEOnFailure", "onFailure: " + t);
                        Toast.makeText(ChangePasswordActivity.this,
                                "Unknown error! Your password hasn't been changed",
                                Toast.LENGTH_SHORT).show();

                    }
                });
    }
}