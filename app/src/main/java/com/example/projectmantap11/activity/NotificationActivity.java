package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.NotificationRecyclerAdapter;
import com.example.projectmantap11.adapter.ReimburseRecyclerAdapter;
import com.example.projectmantap11.models.Notification;
import com.example.projectmantap11.models.NotificationServerResponse;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.ReimbursementServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitNotificationClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity
        implements NotificationRecyclerAdapter.ListItemClickListener {

    private RecyclerView notificationRecycler;
    private ArrayList<Notification> notificationList;
    private boolean isLoading = false;

    private int page = 0;
    private int maxPage = 0;

    private ShimmerFrameLayout mShimmerViewContainer;

    //normal RV
    NotificationRecyclerAdapter mAdapter;

    //is enqueing
    private boolean isEnqueing = false;

    //your key here
    String key = "your key here";
    int maxItem = 0;

    //your uuid here
    String uuid = "your uuid here";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        //HIDE SUPPORTACTIONBAR
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYNOTIFICATION", "loadInitial: " + key);

        //TAKE UUID
        uuid = prefs.getString("useruuid", "empty");
        Log.d("UUIDNOTIFICATION", "onCreate: " + uuid);

        //FINDVIEW
        notificationRecycler = findViewById(R.id.recycler_notification);
        mShimmerViewContainer = findViewById(R.id.shimmer_notification_container);

        //START SHIMMER
        mShimmerViewContainer.startShimmer();

        //setup recyclerview
        notificationRecycler.setLayoutManager(new LinearLayoutManager(this));
        notificationRecycler.setHasFixedSize(true);

        notificationList = new ArrayList<Notification>();
        callEnqueAPI(key);

        mAdapter = new NotificationRecyclerAdapter(this,this );

        mAdapter.setMenuList(notificationList);
        notificationRecycler.setAdapter(mAdapter);

        initScrollListener();
    }

    private void callEnqueAPI(String keya) {
        if (isEnqueing) {
            Log.d("NOTIFICATIONPAGING", "callEnqueAPI: Don't mess up the RV while loading");
        } else {
            isEnqueing = true;
            ProgressDialog progressDoalog = new ProgressDialog(this);
            progressDoalog.setMessage("Loading....");
            progressDoalog.show();
            progressDoalog.setCanceledOnTouchOutside(false);


            RetrofitNotificationClientInstance
                    .getRetrofit()
                    .create(ApiService.class)
                    .getNotification("Bearer " + keya, uuid, 0)
                    .enqueue(new Callback<NotificationServerResponse>() {
                        @Override
                        public void onResponse(Call<NotificationServerResponse> call
                                , Response<NotificationServerResponse> response) {
                            Log.d("NOTIFICATIONRESPONSE", "onResponse: " + response.raw().toString());
                            isEnqueing = false;

                            progressDoalog.dismiss();

                            if (response.body() != null) {

                                try {
                                    Log.d("REIMBURSETRY", "onResponse: GET DATA : "+
                                            response.body().getOutput_schema().getNotifications().getContent());
                                    notificationList.addAll(response.body().getOutput_schema().getNotifications().getContent());
                                    mAdapter.notifyDataSetChanged();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                    maxPage = response.body().getOutput_schema().getNotifications().getTotal_pages();
                                    maxItem = response.body().getOutput_schema().getNotifications().getTotal_elements();
                                    Log.d("REIMBURSETRY", "onResponse: MAX ITEM = "+
                                            maxItem);

                                    if (page < maxPage - 1) {
                                        page = page + 1;
                                    }
                                }catch (Exception e) {
                                    Log.d("REIMBURSECATCH", "onResponse: DATA FAILURE" +
                                            e);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<NotificationServerResponse> call, Throwable t) {
                            Log.d("NOTIFICATIONONFAILURE", "onFailure: " + t);
                            isEnqueing = false;
                        }
                    });
        }
    }

    private void initScrollListener() {
        notificationRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null &&
                            linearLayoutManager.findLastCompletelyVisibleItemPosition() == notificationList.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void loadMore() {
        notificationList.add(null);

        notificationRecycler.post(new Runnable() {
            public void run() {
                mAdapter.notifyItemInserted(notificationList.size() - 1);
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notificationList.remove(notificationList.size() - 1);
                int scrollPosition = notificationList.size();
                mAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                if (maxItem < nextLimit) {
                    nextLimit = maxItem;
                }

                //addmore

                if(currentSize < nextLimit) {
                    callEnqueAPI(key);
                }

                mAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);
    }

    @Override
    public void onListItemClicked(Notification t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}