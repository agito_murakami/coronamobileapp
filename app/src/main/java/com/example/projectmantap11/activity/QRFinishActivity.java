package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.projectmantap11.R;

public class QRFinishActivity extends AppCompatActivity {

    TextView namaMerchantFinishTV, hargaMerchantFinishTV;
    Button finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_finish);

        //findview
        namaMerchantFinishTV = findViewById(R.id.nama_merchant_finish);
        hargaMerchantFinishTV = findViewById(R.id.harga_merchant_finish);
        finishButton = findViewById(R.id.finish_qr_payment_button);

        //getString
        Intent intent = getIntent();
        String namaMerchant = intent.getStringExtra("namamerchant");
        String hargaMerchant = intent.getStringExtra("hargamerchant");

        namaMerchantFinishTV.setText(namaMerchant);
        hargaMerchantFinishTV.setText(hargaMerchant);

        //onclick
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}