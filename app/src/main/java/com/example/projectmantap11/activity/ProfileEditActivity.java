package com.example.projectmantap11.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.ChangeIdentityBodyRequest;
import com.example.projectmantap11.models.VCRequestListServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileEditActivity extends AppCompatActivity {

    //view components
    private Button submitProfileDataButton;
    private EditText emailProfileET, phoneProfileET, addressProfileET;

    //keys and stuffs
    private String key;
    private String userUUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        //hide actionbar
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);
        Toast.makeText(this, "your key is : "+ key, Toast.LENGTH_SHORT).show();

        //findview
        submitProfileDataButton = findViewById(R.id.profile_edit_submit_button);
        emailProfileET = findViewById(R.id.profile_edit_email_edittext);
        phoneProfileET = findViewById(R.id.profile_edit_phone_edittext);
        addressProfileET = findViewById(R.id.profile_edit_address_edittext);

        //onclicks
        submitProfileDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
                String userUUID = sharedPref.getString("useruuid", "empty");
                String userAddress = sharedPref.getString("useraddress", "empty");
                String userEmail = sharedPref.getString("useremail", "empty");
                String userPhone = sharedPref.getString("userphone", "empty");
                String userAddressInput = addressProfileET.getText().toString();
                String userEmailInput = emailProfileET.getText().toString();
                String userPhoneInput = phoneProfileET.getText().toString();

                if (userAddressInput.length() != 0) {
                    userAddress = userAddressInput;
                }

                if (userEmailInput.length() != 0) {
                    userEmail = userEmailInput;
                }

                if (userPhoneInput.length()!=0) {
                    userPhone = userPhoneInput;
                }

                ChangeIdentityBodyRequest submissionEdit = new ChangeIdentityBodyRequest(userUUID,
                        userEmail,
                        userPhone,
                        userAddress);

                callEnqueEditUserAPI(submissionEdit);
            }
        });

    }

    private void callEnqueEditUserAPI(ChangeIdentityBodyRequest a ) {

        ProgressDialog progressDoalog = new ProgressDialog(ProfileEditActivity.this);
        progressDoalog.setMessage("Editing your profile ....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitClientInstance
                .getRetrofitInstance()
                .create(ApiService.class)
                .putChangeIdentity("Bearer " + key, a)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        progressDoalog.dismiss();

                        Log.d("EDIT USER", "onResponse: " + response.raw().toString());
                        if (response.isSuccessful()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileEditActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("User Edited! Please check on your profile page");
                            builder.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileEditActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("User edit failed, please try again!");
                            builder.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("USEREDIT", "onFailure: " + t);

                        progressDoalog.dismiss();
                        Toast.makeText(ProfileEditActivity.this, "An error occured, error : "
                                + t , Toast.LENGTH_SHORT).show();

                    }
                });
    }

}