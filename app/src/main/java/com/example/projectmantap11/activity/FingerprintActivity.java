package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.projectmantap11.R;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class FingerprintActivity extends AppCompatActivity {

    private String BIOMETRIC_TAG = "BIOMETRICTAG";

    private String uuid, yourname, key, userid, branchname;

    private Button confirmLoginnoFPButton;

    private PinView inputpinpinview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);

        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);
        Toast.makeText(this, "your key is : "+ key, Toast.LENGTH_SHORT).show();

        //TAKE SET PIN AND FP KEY
        SharedPreferences sharedPrefsetup = getSharedPreferences("CORONA_SETUP", Context.MODE_PRIVATE);
        boolean isPinSet = sharedPrefsetup.getBoolean("setpin", false);
        boolean isUsingFP = sharedPrefsetup.getBoolean("setFP", false);
        String pintsetup = sharedPrefsetup.getString("loginpin", "empty");

        //FINDVIEWS
        confirmLoginnoFPButton = findViewById(R.id.login_without_fp_button);
        inputpinpinview = findViewById(R.id.inputpinpinview);


        //ONCLICKS
        confirmLoginnoFPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to home

                if (inputpinpinview.getText().toString().equals(pintsetup)) {
                    Intent intent = new Intent(FingerprintActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FingerprintActivity.this);
                    builder.setCancelable(true);
                    builder.setTitle("Wrong PIN! Please try again");
                    builder.setNegativeButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });




        //Create a thread pool with a single thread//
        Executor newExecutor = Executors.newSingleThreadExecutor();

        FragmentActivity activity = this;

        //Start listening for authentication events//
        final BiometricPrompt myBiometricPrompt = new BiometricPrompt(activity, newExecutor
                , new BiometricPrompt.AuthenticationCallback() {

            @Override
            //onAuthenticationError is called when a fatal error occurrs//
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                } else {

                    //Print a message to Logcat//
                    Log.d(BIOMETRIC_TAG, "An unrecoverable error occurred");
                }
            }

            //onAuthenticationSucceeded is called when a fingerprint is matched successfully//
            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                //Print a message to Logcat//
                Log.d(BIOMETRIC_TAG, "Fingerprint recognised successfully");

                //go to home
                Intent intent = new Intent(FingerprintActivity.this, HomeActivity.class);
                startActivity(intent);
            }


            //onAuthenticationFailed is called when the fingerprint doesn’t match//
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();

                //Print a message to Logcat//
                Log.d(BIOMETRIC_TAG, "Fingerprint not recognised");
            }


        });


        if (isUsingFP) {
            //Create the BiometricPrompt instance//
            final BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    //Add some text to the dialog//
                    .setTitle("Place your fingerprint to identify, press cancel to use normal authentication")
                    .setNegativeButtonText("Cancel")
                    //Build the dialog//
                    .build();

            //Assign an onClickListener to the app’s “Authentication” button//
            myBiometricPrompt.authenticate(promptInfo);
        }
        /* USE THIS IF YOU NEED A BUTTON

        findViewById(R.id.launchAuthentication).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

         */


    }
}