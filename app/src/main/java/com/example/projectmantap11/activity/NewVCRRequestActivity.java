package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.CategoryRecyclerAdapter;
import com.example.projectmantap11.helper.InputFilterMinMax;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.KategoriServerResponse;
import com.example.projectmantap11.models.NewVCRBodyRequest;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewVCRRequestActivity extends AppCompatActivity
        implements CategoryRecyclerAdapter.ListItemClickListener{

    //KEYS
    String key = "your key here";

    //VIEWS
    private EditText  description, amount;
    private TextView startDate, endDate;
    private Button submitNewVCR;
    private RecyclerView kategoriRecyclerView;

    //DATEPICKER DIALOG
    private DatePickerDialog picker;
    private DatePickerDialog pickerend;

    //CALENDAR VALUES
    private Calendar cldrStart = Calendar.getInstance();
    private Calendar clrdEnd = Calendar.getInstance();
    private String dateStartString = "empty";
    private Date dateStartDate;

    //PAGINATION FOR CATEGORY RECYCLERVIEW
    private int pageCategory = 0;
    private int maxPageCategory = 0;
    int maxItemCategory = 0;
    boolean isLoading = false;

    //RV STUFFS FOR CATEGORY
    private List<Kategori> kategoriList = new ArrayList<Kategori>();
    private CategoryRecyclerAdapter categoryRecyclerAdapter;
    private int maxAmountPerCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_v_c_r_request);

        //hide supportactionbar
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);

        //FINDVIEWS
        startDate = findViewById(R.id.vcr_request_start_date);
        endDate = findViewById(R.id.vcr_request_end_date);
        description =findViewById(R.id.vcr_request_description);
        amount = findViewById(R.id.vcr_request_amount);
        submitNewVCR = findViewById(R.id.vcr_request_submit_btn);
        kategoriRecyclerView = findViewById(R.id.vcr_request_recycler_kategori);

        //CATEGORY RECYCLERVIEW
        kategoriRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        kategoriRecyclerView.setHasFixedSize(true);
        callEnqueKategoriAPI();
        categoryRecyclerAdapter = new CategoryRecyclerAdapter(this, this);
        categoryRecyclerAdapter.setMenuList(kategoriList);
        kategoriRecyclerView.setAdapter(categoryRecyclerAdapter);
        initScrollListener();

        //AMOUNT LIMITER
        amount.setHint("Please set category before inputing amount");

        //EDITTEXT LISTENER
        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1 && s.toString().startsWith("0")) {
                    s.clear();
                }
            }
        });

        //SETCALENDARDIALOG ONCLICK
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = cldrStart.get(Calendar.DAY_OF_MONTH);
                int month = cldrStart.get(Calendar.MONTH);
                int year = cldrStart.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(NewVCRRequestActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                int monthOfYears = monthOfYear + 1;
                                String monthyear = "monthyearhere";
                                String daymonth = "daymonthhere";

                                if (monthOfYears < 10) {
                                    monthyear = "0" + monthOfYears;
                                } else {
                                    monthyear = String.valueOf(monthOfYears);
                                }

                                if (dayOfMonth < 10) {
                                    daymonth = "0" + dayOfMonth;
                                } else {
                                    daymonth = String.valueOf(dayOfMonth);
                                }

                                dateStartString = year + "-" + monthyear + "-" + daymonth;
                                startDate.setText(dateStartString);
                                dateStartDate = new Date();
                                dateStartDate.setDate(dayOfMonth);
                                dateStartDate.setMonth(monthOfYear);
                                dateStartDate.setYear(year);
                                cldrStart.set(year, monthOfYear, dayOfMonth);
                                clrdEnd.set(year, monthOfYear, dayOfMonth + 1);
                            }
                        }, year, month, day);
                picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                picker.show();

            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dateStartString.equals("empty")) {
                    Toast.makeText(NewVCRRequestActivity.this, "Please set start date first"
                            , Toast.LENGTH_SHORT).show();
                } else {
                    Date x = dateStartDate;
                    int day = clrdEnd.get(Calendar.DAY_OF_MONTH);
                    int month = clrdEnd.get(Calendar.MONTH);
                    int year = clrdEnd.get(Calendar.YEAR);
                    // date picker dialog
                    pickerend = new DatePickerDialog(NewVCRRequestActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int monthOfYears = monthOfYear + 1;
                                    String monthyear = "monthyearhere";
                                    String daymonth = "daymonthhere";

                                    if (monthOfYears < 10) {
                                        monthyear = "0" + monthOfYears;
                                    } else {
                                        monthyear = String.valueOf(monthOfYears);
                                    }

                                    if (dayOfMonth < 10) {
                                        daymonth = "0" + dayOfMonth;
                                    } else {
                                        daymonth = String.valueOf(dayOfMonth);
                                    }

                                    endDate.setText(year + "-" + monthyear + "-" + daymonth);
                                }
                            }, year, month, day);
                    pickerend.getDatePicker().setMinDate(clrdEnd.getTimeInMillis());
                    pickerend.show();
                }


            }
        });

        //BUTTON ON CLICK
        submitNewVCR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CHECK CATEGORY
                if (categoryRecyclerAdapter.getSelected() == null) {
                    Toast.makeText(NewVCRRequestActivity.this
                            , "Please select your category!"
                            , Toast.LENGTH_SHORT).show();
                } else {
                    //SET CATEGORY AND MAX VALUE
                    String kategoryStringInput = categoryRecyclerAdapter.getSelected();

                    //CHECK START DATE
                    if (startDate.getText().equals("")) {
                        Toast.makeText(NewVCRRequestActivity.this
                                , "Please select your start date!"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        //SET START DATE
                        String startDateInput = startDate.getText().toString();

                        //CHECK END DATE
                        if (endDate.getText().equals("")) {
                            Toast.makeText(NewVCRRequestActivity.this
                                    , "Please select your end date!"
                                    , Toast.LENGTH_SHORT).show();
                        } else {
                            //SET END DATE
                            String endDateInput = endDate.getText().toString();

                            //CHECK AMOUNT
                            if (Integer.parseInt(amount.getText().toString()) > maxAmountPerCategory) {
                                Toast.makeText(NewVCRRequestActivity.this
                                        , "Please fix your amount, maximum amount for this category is : " + maxAmountPerCategory
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                //SET AMOUNT
                                String amountInput = amount.getText().toString();

                                //CHECK DESCRIPTION
                                if (description.getText().equals("")) {
                                    Toast.makeText(NewVCRRequestActivity.this
                                            , "Please fill your description!"
                                            , Toast.LENGTH_SHORT).show();
                                } else {
                                    //SET DESCRIPTION
                                    String descriptionInput = description.getText().toString();

                                    NewVCRBodyRequest vcrBodyRequestInput =
                                            new NewVCRBodyRequest(kategoryStringInput,
                                                    startDateInput,
                                                    endDateInput,
                                                    amountInput,
                                                    descriptionInput);

                                    enqueNewVCRRequest(vcrBodyRequestInput);
                                }
                            }
                        }
                    }

                }
            }
        });

    }

    private void enqueNewVCRRequest(NewVCRBodyRequest lalala) {
        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(NewVCRRequestActivity.this);
        progressDoalog.setMessage("Submitting request....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .postNewVCR("Bearer " + key, lalala)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        progressDoalog.dismiss();
                        Log.d("NEWVCRPROGRESS", "onResponse: " + response.raw().toString());
                        if (response.isSuccessful()) {
                            // Do awesome stuff
                            //show alert dialog
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewVCRRequestActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("Submitted!");
                            builder.setMessage("Your virtual card request has been submitted, you'll be redirected to homepage");
                            builder.setPositiveButton("Confirm",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });


                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else if (response.code() == 401) {
                            // Handle unauthorized
                            Toast.makeText(NewVCRRequestActivity.this,
                                    "Unauthorized access! Your request hasn't been submitted",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Handle other responses
                            //String rererere = new Gson().toJson(response);
                            //Log.d("ERRORVCR", "onResponse: " + rererere);
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String rere = jObjError.get("message").toString();

                                Toast.makeText(NewVCRRequestActivity.this, rere, Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                Toast.makeText(NewVCRRequestActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("VCRREQUEST", "onFailure: " + t);
                        Toast.makeText(NewVCRRequestActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void callEnqueKategoriAPI() {
        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(NewVCRRequestActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getDaftarKategori("Bearer " + key, pageCategory, "virtualcard")
                .enqueue(new Callback<KategoriServerResponse>() {
                    @Override
                    public void onResponse(Call<KategoriServerResponse> call, Response<KategoriServerResponse> response) {
                        //Log.d("getrawkategori", "onResponse: " + response.body());

                        progressDoalog.dismiss();

                        if (response.body() != null) {

                            try {
                                Log.d("get raw", "getraw: " + response.raw());
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getOutputSchema().getCategories().getContent());
                                kategoriList.addAll(response.body().getOutputSchema().getCategories().getContent());
                                categoryRecyclerAdapter.notifyDataSetChanged();

                                maxPageCategory = response.body().getOutputSchema().getCategories().getTotal_pages();
                                maxItemCategory = response.body().getOutputSchema().getCategories().getTotal_elements();

                                amount.setFocusable(true);
                                maxAmountPerCategory = categoryRecyclerAdapter.getMaxHarga();
                                String maxAmountCat = String.valueOf(maxAmountPerCategory);
                                amount.setHint("");
                                amount.setFilters(new InputFilter[]{ new InputFilterMinMax("1",maxAmountCat)});

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("ARDA Kategori"
                                        , "onResponse: DATA FAILURE" +
                                                e);
                                Toast.makeText(NewVCRRequestActivity.this, "" + e
                                        , Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<KategoriServerResponse> call, Throwable t) {
                        Log.d("ARDA Kategori", "onFailure: " + t);
                        Toast.makeText(NewVCRRequestActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initScrollListener() {
        kategoriRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == kategoriList.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void loadMore() {
        kategoriList.add(null);
        categoryRecyclerAdapter.notifyItemInserted(kategoriList.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                kategoriList.remove(kategoriList.size() - 1);
                int scrollPosition = kategoriList.size();
                categoryRecyclerAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                if (maxItemCategory < nextLimit) {
                    nextLimit = maxItemCategory;
                }

                //addmore

                if(currentSize < nextLimit) {
                    callEnqueKategoriAPI();
                }

                categoryRecyclerAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);


    }

    @Override
    public void onListItemClicked(Kategori t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}