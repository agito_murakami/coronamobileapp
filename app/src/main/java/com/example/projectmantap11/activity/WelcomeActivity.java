package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.projectmantap11.R;

public class WelcomeActivity extends AppCompatActivity {

    private Button toLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        getSupportActionBar().hide();


        toLogin = findViewById(R.id.to_login_button);
        toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //SHAREDPREF COMMIT
                SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP_WELCOME", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putBoolean("welcomeactivity", true);
                editor.commit();

                Intent toLogin = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(toLogin);
            }
        });
    }
}