package com.example.projectmantap11.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chaos.view.PinView;
import com.cooltechworks.creditcarddesign.CreditCardView;
import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.VirtualCardDetailsTransactionAdapter;
import com.example.projectmantap11.adapter.VirtualCardListAdapter;
import com.example.projectmantap11.models.ActivateVirtualCardBodyRequest;
import com.example.projectmantap11.models.ChangeIdentityBodyRequest;
import com.example.projectmantap11.models.CompanyServerResponse;
import com.example.projectmantap11.models.VCDetailServerResponse;
import com.example.projectmantap11.models.VirtualCard;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitTransactionClientInstance;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VirtualCardDetailsActivity extends AppCompatActivity
    implements VirtualCardDetailsTransactionAdapter.ListItemClickListener {

    private String key;
    private String vcUuid;
    private String userUUID;
    private String cardNumberGlobal;

    private CreditCardView creditCardView;
    private TextView cardNumberTV, cardStatusTV, companyNameTV, cardLimitTV, transactionTV;
    private boolean isEnqueing = false;
    private ImageView companyLogoIV, backButton;
    private ProgressBar progressBarMeterLimit;
    private Button showCVV, activateVC;

    //RECYCLERVIEW TRANSACTIONS
    private RecyclerView transactionVCDetailsRV;

    //LIST FOR VIRTUALCARD
    private ArrayList<Transaction> transactionArrayList;
    private VirtualCardDetailsTransactionAdapter transactionAdapter;

    //PARTS FOR UNLIMITED SCROLLING WORKS
    private boolean isLoading = false;
    private int page = 0;
    private int maxPage = 0;
    int maxItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_card_details);
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYCHANGEPASS", "loadInitial: " + key);

        //TAKE USER UUID
        userUUID = prefs.getString("useruuid", "empty");

        //FINDVIEW
        creditCardView = findViewById(R.id.vcdetails_cardview);
        cardNumberTV = findViewById(R.id.vcdetails_card_number);
        cardStatusTV = findViewById(R.id.vcdetails_status);
        cardLimitTV = findViewById(R.id.vcdetails_limit_amount);
        companyNameTV = findViewById(R.id.vcdetails_company_name);
        companyLogoIV = findViewById(R.id.vcdetails_company_logo);
        progressBarMeterLimit = findViewById(R.id.vcdetails_progress_limit);
        transactionVCDetailsRV = findViewById(R.id.vcdetails_transaction_recyclerview);
        showCVV = findViewById(R.id.vcdetails_show_cvv_button);
        backButton = findViewById(R.id.vcdetails_back_button);
        activateVC = findViewById(R.id.vcdetails_activate_vc_button);
        transactionTV = findViewById(R.id.vcdetails_transaction_textview);

        //GET INTENT
        Intent i = getIntent();
        vcUuid = i.getStringExtra("vcuuid");

        Log.d("VCDETAILS", "onCreate: VC uuid : " + vcUuid);

        //onclicks
        activateVC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                View alertView = getLayoutInflater().inflate(R.layout.custom_alert_activate_vc_view, null);

                //Set the view
                alert.setView(alertView);
                //Show alert
                final AlertDialog alertDialog = alert.show();
                //Can not close the alert by touching outside.
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView closeButton = (ImageView) alertView.findViewById(R.id.custom_alert_activate_vc_close_button);
                Button submitButtonActivation = alertView.findViewById(R.id.custom_alert_activate_vc_confirm_button);
                PinView pinViewNew = alertView.findViewById(R.id.custom_alert_activate_vc_pinview);
                PinView pinViewOld = alertView.findViewById(R.id.custom_alert_activate_vc_pinview_old);
                PinView pinViewConfirm = alertView.findViewById(R.id.custom_alert_activate_vc_pinview_confirm);

                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                submitButtonActivation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String cardUUIDInput = vcUuid;
                        String userUUIDinput = userUUID;
                        String cardNumberInput = cardNumberGlobal;
                        String oldPin = pinViewOld.getText().toString();
                        String newPin = pinViewNew.getText().toString();
                        String confirmPin = pinViewConfirm.getText().toString();

                        if (cardUUIDInput.isEmpty() || cardUUIDInput.equals("empty")) {
                            Toast.makeText(VirtualCardDetailsActivity.this, "ERROR! Your card UUID is empty", Toast.LENGTH_SHORT).show();
                        } else {
                            if (userUUIDinput.isEmpty() || userUUIDinput.equals("empty")) {
                                Toast.makeText(VirtualCardDetailsActivity.this, "ERROR! Your UUID is empty", Toast.LENGTH_SHORT).show();
                            } else {
                                if (cardNumberInput.isEmpty() || cardNumberInput.equals("empty")) {
                                    Toast.makeText(VirtualCardDetailsActivity.this, "ERROR! Your card number is empty", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (oldPin.length() != 6 || newPin.length() != 6 || confirmPin.length() != 6) {
                                        Toast.makeText(VirtualCardDetailsActivity.this, "ERROR! All field must be filled", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (!newPin.equals(confirmPin)) {
                                            Toast.makeText(VirtualCardDetailsActivity.this, "ERROR! New and confirmation PIN must be 6 digits each", Toast.LENGTH_SHORT).show();
                                        } else {
                                            ActivateVirtualCardBodyRequest input = new ActivateVirtualCardBodyRequest(cardUUIDInput,
                                                    userUUIDinput,cardNumberGlobal,oldPin,newPin);

                                            alertDialog.dismiss();

                                            callActivateVC(input);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });

            }
        });

        showCVV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                View alertView = getLayoutInflater().inflate(R.layout.custom_alert_pin_view, null);

                //Set the view
                alert.setView(alertView);
                //Show alert
                final AlertDialog alertDialog = alert.show();
                //Can not close the alert by touching outside.
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView closeButton = (ImageView) alertView.findViewById(R.id.custom_alert_close_button);
                Button submitButton = alertView.findViewById(R.id.custom_alert_confirm_button);
                PinView pinViewAlert = alertView.findViewById(R.id.custom_alert_pinview);

                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String pin  = pinViewAlert.getText().toString();

                        if (pin.length() != 6) {
                            Toast.makeText(VirtualCardDetailsActivity.this, "Please provide proper PIN number"
                                    , Toast.LENGTH_SHORT).show();
                        } else {

                            try {
                                KeyGenerator keyGenerator;
                                SecretKey secretKey;
                                keyGenerator = KeyGenerator.getInstance("AES");
                                keyGenerator.init(256);
                                secretKey = keyGenerator.generateKey();

                                byte[] IV = new byte[16];
                                SecureRandom random;
                                random = new SecureRandom();
                                random.nextBytes(IV);
                            } catch (NoSuchAlgorithmException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //INITIATE VIRTUAL CARD LIST
        transactionArrayList = new ArrayList<Transaction>();
        transactionAdapter = new VirtualCardDetailsTransactionAdapter(this,this );
        transactionAdapter.setMenuList(transactionArrayList);
        transactionVCDetailsRV.setAdapter(transactionAdapter);

        callEnqueVCDetails(vcUuid);

    }

    @Override
    protected void onResume() {
        super.onResume();
        transactionVCDetailsRV.setLayoutManager(new LinearLayoutManager(this));
        transactionVCDetailsRV.setHasFixedSize(true);

        transactionArrayList = new ArrayList<Transaction>();
        callEnqueVCDetails(vcUuid);
        transactionAdapter = new VirtualCardDetailsTransactionAdapter(this,this );
        transactionAdapter.setMenuList(transactionArrayList);
        transactionVCDetailsRV.setAdapter(transactionAdapter);
    }

    private void callEnqueVCDetails(String a ) {

        ProgressDialog progressDoalog = new ProgressDialog(VirtualCardDetailsActivity.this);
        progressDoalog.setMessage("Loading your card ....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitTransactionClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getVirtualCardDetail("Bearer " + key, a)
                .enqueue(new Callback<VCDetailServerResponse>() {
                    @Override
                    public void onResponse(Call<VCDetailServerResponse> call, Response<VCDetailServerResponse> response) {

                        progressDoalog.dismiss();

                        Log.d("VCDETAILS", "onResponse: " + response.raw().toString());
                        if (response.isSuccessful()) {
                            Log.d("VCDETAILS", "onResponse: RESPONSE SUCCESSFUL");

                            //retrive needed data for VC
                            String cardNumber = response.body().getOutput_schema().getVirtual_card()
                                    .getCard_number();

                            String cardStatus = response.body().getOutput_schema().getVirtual_card()
                                    .getStatus();

                            String cardUser = response.body().getOutput_schema().getUser().getName();

                            String cardEndDate = response.body().getOutput_schema().getVirtual_card()
                                    .getEnd_date();

                            String cardLimit = response.body().getOutput_schema().getVirtual_card()
                                    .getLimit_transaction();

                            String cardLimitUsed = response.body().getOutput_schema().getVirtual_card()
                                    .getLimit_used();

                            //date formatting
                            String[] cardFormat1 = cardEndDate.split("T");
                            String cardFormat1Take = cardFormat1[0];
                            String[] cardFormat2 = cardFormat1Take.split("-");
                            String[] yearFormat = cardFormat2[0].split("(?<=\\G.{2})");
                            String cardFormat2Take =  cardFormat2[1] + yearFormat[1];

                            //limit formatting
                            double amount = Double.parseDouble(cardLimit);
                            double amountUsed = Double.parseDouble(cardLimitUsed);
                            Locale indo = new Locale("id", "ID");
                            NumberFormat indoFormat = NumberFormat.getCurrencyInstance(indo);
                            String limitShow = indoFormat.format(amount);
                            String limitUsedShow = indoFormat.format(amountUsed);
                            String limitFinalShow = limitUsedShow + " / " + limitShow;
                            int percentage = (int) Math.ceil(amountUsed/amount*100);
                            Log.d("VCDETAILS", "onResponse: percentage " + percentage);

                            progressBarMeterLimit.setProgress(percentage);

                            //setup progressbar,
                            if (percentage >= 80) {
                                progressBarMeterLimit
                                        .setProgressDrawable(ContextCompat
                                                .getDrawable(VirtualCardDetailsActivity
                                                        .this,R.drawable.background_progress_bar_red));
                            } else if (percentage < 79 && percentage >= 60 ) {
                                progressBarMeterLimit
                                        .setProgressDrawable(ContextCompat
                                                .getDrawable(VirtualCardDetailsActivity
                                                        .this,R.drawable.background_progress_bar_orange));
                            } else if (percentage < 59 && percentage >= 40 ) {
                                progressBarMeterLimit
                                        .setProgressDrawable(ContextCompat
                                                .getDrawable(VirtualCardDetailsActivity
                                                        .this,R.drawable.background_progress_bar_yellow));
                            } else {
                                progressBarMeterLimit
                                        .setProgressDrawable(ContextCompat
                                                .getDrawable(VirtualCardDetailsActivity
                                                        .this,R.drawable.background_progress_bar_green));
                            }


                            cardNumberGlobal = cardNumber;
                            cardNumberTV.setText(cardNumber);
                            creditCardView.setCardNumber(cardNumber);
                            creditCardView.setCardHolderName(cardUser);
                            creditCardView.setCardExpiry(cardFormat2Take);
                            cardLimitTV.setText(limitFinalShow);

                            if (cardStatus.equals("RELEASED")) {
                                cardStatusTV.setText(cardStatus);
                                cardStatusTV.setTextColor(Color.BLUE);
                            } else {
                                cardStatusTV.setText(cardStatus);
                            }

                            //fillTRANSACTION RV
                            transactionArrayList.addAll(response.body().getOutput_schema().getTransactions());
                            transactionAdapter.notifyDataSetChanged();

                            if (response.body().getOutput_schema().getVirtual_card().getStatus().equals("RELEASED")) {
                                //HIDE BOTH
                                transactionTV.setVisibility(View.GONE);
                                transactionVCDetailsRV.setVisibility(View.GONE);
                            } else {
                                activateVC.setVisibility(View.GONE);
                            }

                            callEnqueMyCompanyAPI();

                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("User load failed, please try again!");
                            builder.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    }

                    @Override
                    public void onFailure(Call<VCDetailServerResponse> call, Throwable t) {
                        Log.d("USEREDIT", "onFailure: " + t);

                        progressDoalog.dismiss();
                        Toast.makeText(VirtualCardDetailsActivity.this, "An error occured, error : "
                                + t , Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void callActivateVC(ActivateVirtualCardBodyRequest input) {
        ProgressDialog progressDoalog = new ProgressDialog(VirtualCardDetailsActivity.this);
        progressDoalog.setMessage("Loading your card ....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitTransactionClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .putActivateVirtualCard("Bearer " + key, input)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            Log.d("ACTIVATEVC", "onResponse: RESPONSE SUCCESSFUL");
                            AlertDialog.Builder builder = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("Activated!");
                            builder.setMessage("Card successfully activated! You'll be redirected to your virtual card list");
                            builder.setPositiveButton("Confirm",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            Log.d("ACTIVATEVC", "onResponse: RESPONSE NOT SUCCESSFUL");
                            AlertDialog.Builder builder = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("Activated!");
                            builder.setMessage("Activation failed! Please check your code");
                            builder.setPositiveButton("Confirm",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("ACTIVATEVC", "onFailure: RESPONSE FAILURE");
                        AlertDialog.Builder builder = new AlertDialog.Builder(VirtualCardDetailsActivity.this);
                        builder.setCancelable(true);
                        builder.setTitle("Activated!");
                        builder.setMessage("Unknown error occurred, please try again");
                        builder.setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
    }

    private void callEnqueMyCompanyAPI() {

        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(VirtualCardDetailsActivity.this);
        progressDoalog.setMessage("Loading your company data....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        if (!isEnqueing) {
            isEnqueing = true;
            RetrofitClientInstance
                    .getRetrofitInstance()
                    .create(ApiService.class)
                    .getCompanyData("Bearer " + key)
                    .enqueue(new Callback<CompanyServerResponse>() {
                        @Override
                        public void onResponse(Call<CompanyServerResponse> call, Response<CompanyServerResponse> response) {
                            progressDoalog.dismiss();
                            isEnqueing = false;
                            Log.d("COMPANYDATA", "onResponse: " + response.raw().toString());

                            if (response.body() != null) {

                                try {
                                    String companyName = response.body().getOutput_schema().getCompany().getName();
                                    String companyLogoPath = response.body().getOutput_schema().getCompany().getLogo();

                                    companyNameTV.setText(companyName);
                                    Picasso.get().load(companyLogoPath).into(companyLogoIV);
                                }catch (Exception e) {
                                    Log.d("COMPANYDATA", "onResponse: DATA FAILURE" +
                                            e);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CompanyServerResponse> call, Throwable t) {
                            progressDoalog.dismiss();
                            Log.d("COMPANYDATA", "onFailure: " + t);
                            isEnqueing = false;

                        }
                    });
        } else {
            progressDoalog.dismiss();
            //LOGGING
            Log.d("COMPANYDATA", "callEnqueAPI: HIT PAGE still hitting, don't mess up the LOAD please");
        }
    }

    public static byte[] encrypt(byte[] plaintext, SecretKey key, byte[] IV) throws Exception
    {
        Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        byte[] cipherText = cipher.doFinal(plaintext);
        return cipherText;
    }

    public static String decrypt(byte[] cipherText, SecretKey key, byte[] IV)
    {
        try {
            Cipher cipher = Cipher.getInstance("AES");
            SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(IV);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] decryptedText = cipher.doFinal(cipherText);
            return new String(decryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onListItemClicked(VirtualCard t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}