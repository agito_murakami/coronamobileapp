package com.example.projectmantap11.activity;

public class Transaction {
    private String uuid;
    private String company_id;
    private String branch_uuid;
    private String division_uuid;
    private String user_uuid;
    private String virtual_card;
    private String merchant;
    private String account_number;
    private double amount;
    private String coordinate_start;
    private String coordinate_end;
    private boolean coordinate_valid;
    private boolean transaction_success;
    private boolean flag_qr;
    private String created_date;
    private String modify_date;
    private String card_number;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getBranch_uuid() {
        return branch_uuid;
    }

    public void setBranch_uuid(String branch_uuid) {
        this.branch_uuid = branch_uuid;
    }

    public String getDivision_uuid() {
        return division_uuid;
    }

    public void setDivision_uuid(String division_uuid) {
        this.division_uuid = division_uuid;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getVirtual_card() {
        return virtual_card;
    }

    public void setVirtual_card(String virtual_card) {
        this.virtual_card = virtual_card;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCoordinate_start() {
        return coordinate_start;
    }

    public void setCoordinate_start(String coordinate_start) {
        this.coordinate_start = coordinate_start;
    }

    public String getCoordinate_end() {
        return coordinate_end;
    }

    public void setCoordinate_end(String coordinate_end) {
        this.coordinate_end = coordinate_end;
    }

    public boolean isCoordinate_valid() {
        return coordinate_valid;
    }

    public void setCoordinate_valid(boolean coordinate_valid) {
        this.coordinate_valid = coordinate_valid;
    }

    public boolean isTransaction_success() {
        return transaction_success;
    }

    public void setTransaction_success(boolean transaction_success) {
        this.transaction_success = transaction_success;
    }

    public boolean isFlag_qr() {
        return flag_qr;
    }

    public void setFlag_qr(boolean flag_qr) {
        this.flag_qr = flag_qr;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }
}
