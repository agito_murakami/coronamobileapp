package com.example.projectmantap11.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.projectmantap11.R;
import com.example.projectmantap11.fragment.BottomSheetFragment;
import com.example.projectmantap11.models.Karyawan;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private String key = "your auth key here";
    private String uuid = "your uuid key here";
    private String employeeid = "your userID here";
    private String branchid = "your branch id here";
    private String branchname = "your branch name here";
    private String yourname = "your name here";
    private String companyid = "your company ID here";

    public static Context contextOfApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //HIDE SUPPORT ACTION BAR
        getSupportActionBar().hide();


        //context
        contextOfApplication = getApplicationContext();

        //bottom navigation menu
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_profile, R.id.navigation_more)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);

        //TAKE OTHER VALUES
        uuid = prefs.getString("useruuid", "empty");
        employeeid = prefs.getString("employeeid", "empty");
        yourname = prefs.getString("employeename", "empty");
        String divisionID = prefs.getString("divisionid", "empty");
        String divisionName = prefs.getString("divisionname", "empty");
        String branchID = prefs.getString("branchid", "empty");
        branchname = prefs.getString("branchname", "empty");
        companyid = prefs.getString("companyid", "empty");

        Toast.makeText(this, "your key is : "+ key
                + "\n" + "your uuid is : " + uuid
                + "\n" + "your employee ID is : " + employeeid
                + "\n" + "your name is : " + yourname
                + "\n" + "your divisionID is : " + divisionID
                + "\n" + "your divisionName is : " + divisionName
                + "\n" + "your branch ID is : " + branchID
                + "\n" + "your branchname is : " + branchname
                + "\n" + "your companyid is : " + companyid, Toast.LENGTH_SHORT).show();

    }

    private void getAllKaryawan() {
        try {
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<List<Karyawan>> call = service
                    .getDaftarAllKaryawan("Bearer "+key);

            call.enqueue(new Callback<List<Karyawan>>() {

                @Override
                public void onResponse(Call<List<Karyawan>> call, Response<List<Karyawan>> response) {
                    try {
                        for (Karyawan l : response.body()) {
                            Log.d("KARYAWAN", "onResponse: " + l.getName());
                        }
                    } catch (Exception e) {
                        Log.d("ERROR TAG ", "onResponse: " + e);
                    }
                }

                @Override
                public void onFailure(Call<List<Karyawan>> call, Throwable t) {
                    Log.d("GAGAL MANING BOS", "onFailure: ");
                }
            });
        } catch (Exception e) {
            Log.d("API ERROR", "onCreate: " + e);
        }
    }
}