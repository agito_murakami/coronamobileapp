package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.ReimburseRecyclerAdapter;
import com.example.projectmantap11.adapter.VirtualCardListAdapter;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.VCListServerResponse;
import com.example.projectmantap11.models.VirtualCard;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitTransactionClientInstance;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VirtualCardListActivity extends AppCompatActivity
        implements VirtualCardListAdapter.ListItemClickListener{

    private RecyclerView virtualCardRV;

    private String key;

    //LIST FOR VIRTUALCARD
    private ArrayList<VirtualCard> virtualCardList;
    private VirtualCardListAdapter virtualCardListAdapter;

    //PARTS FOR UNLIMITED SCROLLING WORKS
    private boolean isLoading = false;
    private int page = 0;
    private int maxPage = 0;
    int maxItem = 0;
    private boolean isEnqueing = false;

    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_card_list);

        //HIDE SUPPORT ACTION BAR
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("VIRTUALCARDLIST", "loadInitial: " + key);

        //FINDVIEW
        virtualCardRV = findViewById(R.id.recycler_vcard_history);
        mShimmerViewContainer = findViewById(R.id.virtual_card_list_shimmer_container);

        //INITIATE VIRTUAL CARD LIST
        virtualCardList = new ArrayList<VirtualCard>();
        hitGetAllVC();
        virtualCardListAdapter = new VirtualCardListAdapter(this,this );
        virtualCardListAdapter.setMenuList(virtualCardList);
        virtualCardRV.setAdapter(virtualCardListAdapter);

        //START SHIMMER
        mShimmerViewContainer.startShimmer();
        //INIT RV
        initScrollListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //setup recyclerview
        virtualCardRV.setLayoutManager(new LinearLayoutManager(this));
        virtualCardRV.setHasFixedSize(true);

        virtualCardList = new ArrayList<VirtualCard>();
        hitGetAllVC();
        virtualCardListAdapter = new VirtualCardListAdapter(this,this );
        virtualCardListAdapter.setMenuList(virtualCardList);
        virtualCardRV.setAdapter(virtualCardListAdapter);
        initScrollListener();

    }

    //--------------------------API----------------------------
    //pay API void
    private void hitGetAllVC() {

        if (!isEnqueing) {
            isEnqueing = true;
            //Start hitting the VC List API
            RetrofitTransactionClientInstance
                    .getRetrofit()
                    .create(ApiService.class)
                    .getDaftarVC("Bearer " + key)
                    .enqueue(new Callback<VCListServerResponse>() {
                        @Override
                        public void onResponse(Call<VCListServerResponse> call, Response<VCListServerResponse> response) {
                            Log.d("VCLIST", "onResponse: " + response.raw().toString());

                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);

                            isEnqueing = false;

                            if (response.body() != null) {
                                Log.d("VCLIST", "onResponse: body is not null");

                                Log.d("VCLIST", "getraw: " + response.raw());
                                Log.d("VCLIST", "onResponse: GET DATA : "+
                                        response.body().getOutput_schema().getVirtual_cards().getContent());
                                virtualCardList.addAll(response.body().getOutput_schema().getVirtual_cards().getContent());

                                virtualCardListAdapter.notifyDataSetChanged();
                                maxPage = response.body().getOutput_schema().getVirtual_cards().getTotal_pages();
                                maxItem = response.body().getOutput_schema().getVirtual_cards().getTotal_elements();

                                if (page < maxPage - 1) {
                                    page = page + 1;
                                }
                            } else {
                                Log.d("VCLIST", "onResponse: body is null ");
                            }
                        }

                        @Override
                        public void onFailure(Call<VCListServerResponse> call, Throwable t) {
                            Log.d("VCLIST", "onFailure: " + t);
                            virtualCardList.add(null);
                            Toast.makeText(VirtualCardListActivity.this, "" + t
                                    , Toast.LENGTH_SHORT).show();

                            isEnqueing = false;
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                        }
                    });
        } else {
            Log.d("VCLIST", "hitGetAllVC: DON'T BREAK THE RV PLEASE");
        }


    }


    //--------------POPULATE DATA -------------------

    private void initScrollListener() {
        virtualCardRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == virtualCardList.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        virtualCardList.add(null);
        virtualCardListAdapter.notifyItemInserted(virtualCardList.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                virtualCardList.remove(virtualCardList.size() - 1);
                int scrollPosition = virtualCardList.size();
                virtualCardListAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                Log.d("REIMBURSEPAGING", "run: currentsize = "  + currentSize);
                Log.d("REIMBURSEPAGING", "run: maxitem = "  + maxItem);
                Log.d("REIMBURSEPAGING", "run: page = "  + page);

                Log.d("REIMBURSEPAGING", "run: scrollposition = "  + scrollPosition);

                if (currentSize > maxItem) {
                    currentSize = maxItem;
                }

                if (maxItem < nextLimit) {
                    nextLimit = maxItem;
                }

                //addmore

                if(currentSize < nextLimit) {
                    hitGetAllVC();
                }

                virtualCardListAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);
    }

    @Override
    public void onListItemClicked(VirtualCard t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}