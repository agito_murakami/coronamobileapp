package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.projectmantap11.R;

public class AppInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);

        //hideactionbar
        getSupportActionBar().hide();
    }
}