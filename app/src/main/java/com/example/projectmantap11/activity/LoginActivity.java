package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmantap11.BuildConfig;
import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Authorization;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.example.projectmantap11.activity.HomeActivity.contextOfApplication;

public class LoginActivity extends AppCompatActivity {

    private Button submit, testPlane, skip;
    private Context context;
    ProgressDialog progressDoalog;
    private TextInputEditText pass, id;

    private String bearerkey = "Bearer ";

    private String idasker = "mobileapp";
    private String passasker = "abcd";

    //location views
    TextView txtLocationResult;
    TextView txtUpdatedOn;
    Button btnStartUpdates;
    Button btnStopUpdates;
    Button getLastLocBtn;


    //permissioncodes
    int permissions_code = 42;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION
            , Manifest.permission.INTERNET};

    //locationstring
    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //hide action bar
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        setContentView(R.layout.activity_login);

        //prompt permissions
        ActivityCompat.requestPermissions(this,
                permissions,
                permissions_code);

        context = this.getApplicationContext();

        //findview
        txtLocationResult = findViewById(R.id.location_result);
        txtUpdatedOn = findViewById(R.id.updated_on);
        btnStartUpdates = findViewById(R.id.btn_start_location_updates);
        btnStopUpdates = findViewById(R.id.btn_stop_location_updates);
        getLastLocBtn = findViewById(R.id.btn_get_last_location);
        //login mechanism
        id = findViewById(R.id.login_user_id_input);
        pass = findViewById(R.id.login_password_input);
        testPlane = findViewById(R.id.test_plane_button);
        skip = findViewById(R.id.skip_login_button);
        submit = findViewById(R.id.loginbutton);

        //init location
        initLocation();
        // restore the values from saved instance state
        restoreValuesFromBundle(savedInstanceState);

        //start location updates :
        startLocationButtonClick();


        //Onclicks :
        //btnstart, stop, and getlastloc, TESTPLANE, and skip button,
        // are testing buttons. REMOVE WHEN RELEASING
        btnStartUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationButtonClick();
            }
        });

        btnStopUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        getLastLocBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLastKnownLocation();
            }
        });

        testPlane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goTestPlane = new Intent(LoginActivity.this, TestPlaneActivity.class);
                startActivity(goTestPlane);
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent skip = new Intent(LoginActivity.this, FingerprintActivity.class);
                //CHECK IF PIN IS SET
                SharedPreferences sharedPrefsetup = getSharedPreferences("CORONA_SETUP", Context.MODE_PRIVATE);
                boolean isPinSet = sharedPrefsetup.getBoolean("setpin", false);
                boolean isUsingFP = sharedPrefsetup.getBoolean("setFP", false);

                if (isPinSet) { //PIN SET
                    if (isUsingFP) { //PIN SET, FP SET
                        startActivity(skip);
                    } else { //PIN SET, FP NOT SET, show alert dialog FOR SETTING FP
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setCancelable(true);
                        builder.setTitle("Do you want to use fingerprint for login?");
                        builder.setMessage("Please select : ");
                        builder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //SHAREDPREF COMMIT
                                        SharedPreferences.Editor editor = sharedPrefsetup.edit();
                                        editor.putBoolean("setFP", true);
                                        editor.commit();

                                        startActivity(skip);
                                        finish();
                                    }
                                });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(skip);
                                finish();
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                } else { //PIN NOT SET
                    Intent intentsetpin = new Intent(LoginActivity.this, SetPINActivity.class);
                    startActivity(intentsetpin);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //stop and get last location
                stopLocationButtonClick();
                showLastKnownLocation();

                String idstring = id.getText().toString();
                String passstring = pass.getText().toString();


                String bkey = idasker + ":" + passasker;

                try {
                    bearerkey = encryptingString(bkey);
                    Log.d("YOUR BEARER KEY", "onCreate: " + bearerkey);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.d("ERROR PARSING", "onCreate: " + e);
                }

                progressDoalog = new ProgressDialog(LoginActivity.this);
                progressDoalog.setMessage("Loading....");
                progressDoalog.show();
                progressDoalog.setCanceledOnTouchOutside(false);

                /*Create handle for the RetrofitInstance interface*/
                ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                RequestBody body = RequestBody.create(MediaType.parse("text/plain"), "password");
                RequestBody username = RequestBody.create(MediaType.parse("text/plain"), idstring);
                RequestBody password = RequestBody.create(MediaType.parse("text/plain"), passstring);
                String authkey = "Basic " + bearerkey;
                Call<Authorization> call = service
                        .loginAccount(authkey,
                                body,
                                username,
                                password);

                try {
                    call.enqueue(new Callback<Authorization>() {
                        @Override
                        public void onResponse(Call<Authorization> call, retrofit2.Response<Authorization> response) {
                            progressDoalog.dismiss();

                            if (response.isSuccessful()) {


                                Log.d("AUTHCLEAR", "onResponse: " + response.body());

                                Intent intentCheckFP = new Intent(LoginActivity.this, FingerprintActivity.class);

                                //SHAREDPREF COMMIT
                                SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();

                                editor.putString("key", response.body().getAccessToken());
                                editor.putString("useruuid", response.body().getUuid());
                                editor.putString("employeeid", response.body().getEmployeeID());
                                editor.putString("employeename", response.body().getName());
                                editor.putString("divisionid", response.body().getDivisionID());
                                editor.putString("divisionname", response.body().getDivisionName());
                                editor.putString("branchid", response.body().getBranchID());
                                editor.putString("branchname", response.body().getBranchName());
                                editor.commit();


                                //CHECK IF PIN IS SET
                                SharedPreferences sharedPrefsetup = getSharedPreferences("CORONA_SETUP", Context.MODE_PRIVATE);
                                boolean isPinSet = sharedPrefsetup.getBoolean("setpin", false);
                                boolean isUsingFP = sharedPrefsetup.getBoolean("setFP", false);

                                if (isPinSet) { //PIN SET
                                    if (isUsingFP) { //PIN SET, FP SET
                                        startActivity(intentCheckFP);
                                    } else { //PIN SET, FP NOT SET, show alert dialog FOR SETTING FP
                                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                        builder.setCancelable(true);
                                        builder.setTitle("Do you want to use fingerprint for login?");
                                        builder.setMessage("Please select : ");
                                        builder.setPositiveButton("Yes",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        //SHAREDPREF COMMIT
                                                        SharedPreferences.Editor editor = sharedPrefsetup.edit();

                                                        editor.putBoolean("setFP", true);
                                                        editor.commit();

                                                        startActivity(intentCheckFP);

                                                        finish();
                                                    }
                                                });

                                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity(intentCheckFP);
                                            }
                                        });

                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }
                                } else { //PIN NOT SET
                                    Intent intentsetpin = new Intent(LoginActivity.this, SetPINActivity.class);
                                    startActivity(intentsetpin);
                                }
                            } else {
                                Toast.makeText(LoginActivity.this
                                        , "Login failed! Error code is : " + response.errorBody(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Authorization> call, Throwable t) {
                            progressDoalog.dismiss();
                            Log.d("FAILURE LOGIN", "onFailure: " + t);
                            Toast.makeText(LoginActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    Log.e("API ERROR", "onClick login button: ", e);
                }
            }
        });

        //
    }

    //-------------------------- CALCULATION DISTANCE FUNCTION ---------------------------
    private Double getDistanceFromLatLonInKm(Double lat1, Double lon1, Double lat2, Double lon2) {
        int R = 6371; // Radius of the earth in km
        Double oneint = lat2-lat1;
        Double dLat = deg2rad(oneint);  // deg2rad below
        Double dLon = deg2rad(lon2-lon1);
        Double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double d = R * c; // Distance in km
        return d;
    }

    private Double deg2rad(Double deg) {
        return deg * (Math.PI/180);
    }
    //========================= CALCULATION DISTANCE FUNCTION ==========================


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this
                            , "Permission denied to read your External storage/" +
                                    "your internet / " +
                                    "your GPS", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private String encryptingString(String a) throws UnsupportedEncodingException {
        byte[] data = a.getBytes("UTF-8");
        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);

        return base64;
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            txtLocationResult.setText(
                    "Lat: " + mCurrentLocation.getLatitude() + ", " +
                            "Lng: " + mCurrentLocation.getLongitude()
            );

            // giving a blink animation on TextView
            txtLocationResult.setAlpha(0);
            txtLocationResult.animate().alpha(1).setDuration(300);

            // location last updated time
            txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);
        }

        toggleButtons();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    private void toggleButtons() {
        if (mRequestingLocationUpdates) {
            btnStartUpdates.setEnabled(false);
            btnStopUpdates.setEnabled(true);
        } else {
            btnStartUpdates.setEnabled(true);
            btnStopUpdates.setEnabled(false);
        }
    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("satisfiedsettings", "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("notsatisfied", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(LoginActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("unable to exec", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("error", errorMessage);

                                Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void stopLocationButtonClick() {
        mRequestingLocationUpdates = false;
        stopLocationUpdates();
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                        toggleButtons();
                    }
                });
    }

    public void showLastKnownLocation() {
        if (mCurrentLocation != null) {
            Toast.makeText(getApplicationContext(), "Lat: " + mCurrentLocation.getLatitude()
                    + ", Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG).show();

            //save required data
            SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            String coor1 = mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
            editor.putString("coordinatestart", coor1);
            editor.putLong("timestampstart", System.currentTimeMillis()/1000);
            editor.commit();

            Log.d("COORDINATE", "showLastKnownLocation: coor 1 : " + coor1);
            Log.d("COORDINATE", "showLastKnownLocation: TIME 1 : " + System.currentTimeMillis()/1000);
        } else {
            Toast.makeText(getApplicationContext(), "Last known location is not available!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e("Agree tag", "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e("disacgreetag", "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        openSettings();
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }

        updateLocationUI();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }

}