package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.projectmantap11.R;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.KategoriServerResponse;
import com.example.projectmantap11.models.VCRequestListServerResponse;
import com.example.projectmantap11.models.VirtualCard;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitResoClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestPlaneActivity extends AppCompatActivity {

    private boolean isLoading = false;

    private int pageCategory = 0;
    private int maxPageCategory = 0;
    int maxItemCategory = 0;

    //your key here
    String key = "your key here";

    //view
    private TextView viewText;
    private Button getCategory, getVc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_plane);

        //findview
        viewText = findViewById(R.id.viewtext);
        getCategory = findViewById(R.id.hit_get_category);
        getVc = findViewById(R.id.hit_get_vc);

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);

        //onclick
        getCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEnqueKategoriAPI();
            }
        });

        getVc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callEnqueVirtualCardAPI();
            }
        });
    }

    private void callEnqueKategoriAPI() {
        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getDaftarKategori("Bearer " + key, pageCategory, null)
                .enqueue(new Callback<KategoriServerResponse>() {
                    @Override
                    public void onResponse(Call<KategoriServerResponse> call, Response<KategoriServerResponse> response) {
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());

                        if (response.body() != null) {

                            try {
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getOutputSchema().getCategories().getContent());
                                String outputTextView = null;

                                for (Kategori k : response.body().getOutputSchema().getCategories().getContent()) {
                                    outputTextView = outputTextView + "\n" + k.getName() + "\n";
                                }

                                viewText.setText(outputTextView);

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("TestPlaneKategori", "onResponse: DATA FAILURE" +
                                        e);
                                viewText.setText(e.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<KategoriServerResponse> call, Throwable t) {
                        Log.d("TestPlaneKategori", "onFailure: " + t);


                    }
                });
    }


    /*
    private void callEnqueVirtualCardAPI() {
        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .daftarActiveVC("Bearer " + key)
                .enqueue(new Callback<VCRequestListServerResponse>() {
                    @Override
                    public void onResponse(Call<VCRequestListServerResponse> call, Response<VCRequestListServerResponse> response) {
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());

                        if (response.body() != null) {

                            try {
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getContent());
                                String outputTextView = null;

                                int i = 1;

                                for (VirtualCard k : response.body().getContent()) {
                                    outputTextView = outputTextView + "\n" + i + ":";
                                    outputTextView = outputTextView + k.getCard_number() + "\n";
                                    outputTextView = outputTextView + k.getBranch_uuid() + "\n";
                                    outputTextView = outputTextView + k.getCompany_id() + "\n";
                                    outputTextView = outputTextView + k.getName() + "\n";
                                    outputTextView = outputTextView + k.getUser_uuid() + "\n";
                                    outputTextView = outputTextView + k.getEnd_date() + "\n";
                                    outputTextView = outputTextView + "\n";

                                    i = i+1;
                                }

                                viewText.setText(outputTextView);

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("TestPlaneKategori", "onResponse: DATA FAILURE" +
                                        e);
                                viewText.setText(e.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VCRequestListServerResponse> call, Throwable t) {
                        Log.d("TestPlaneKategori", "onFailure: " + t);


                    }
                });
    }

     */

    /*
    private void callEnqueTrasactionAPI() {
        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .daftarActiveVC("Bearer " + key)
                .enqueue(new Callback<VCRequestListServerResponse>() {
                    @Override
                    public void onResponse(Call<VCRequestListServerResponse> call, Response<VCRequestListServerResponse> response) {
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());

                        if (response.body() != null) {

                            try {
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getContent());
                                String outputTextView = null;

                                int i = 1;

                                for (VirtualCard k : response.body().getContent()) {
                                    outputTextView = outputTextView + "\n" + i + ":";
                                    outputTextView = outputTextView + k.getCard_number() + "\n";
                                    outputTextView = outputTextView + k.getBranch_uuid() + "\n";
                                    outputTextView = outputTextView + k.getCompany_id() + "\n";
                                    outputTextView = outputTextView + k.getName() + "\n";
                                    outputTextView = outputTextView + k.getUser_uuid() + "\n";
                                    outputTextView = outputTextView + k.getEnd_date() + "\n";
                                    outputTextView = outputTextView + "\n";

                                    i = i+1;
                                }

                                viewText.setText(outputTextView);

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("TestPlaneKategori", "onResponse: DATA FAILURE" +
                                        e);
                                viewText.setText(e.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VCRequestListServerResponse> call, Throwable t) {
                        Log.d("TestPlaneKategori", "onFailure: " + t);


                    }
                });
    }

     */
}