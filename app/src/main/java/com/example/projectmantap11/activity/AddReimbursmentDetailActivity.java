package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.projectmantap11.BuildConfig;
import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.CategoryRecyclerAdapter;
import com.example.projectmantap11.helper.InputFilterMinMax;
import com.example.projectmantap11.models.GetOCRPriceBodyRequest;
import com.example.projectmantap11.models.Kategori;
import com.example.projectmantap11.models.KategoriServerResponse;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.ReimbursementBodyPost;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitOCRClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReimbursmentDetailActivity extends AppCompatActivity
        implements CategoryRecyclerAdapter.ListItemClickListener{

    //key
    String key = "your key here";

    //submit data
    String kategoryString = "your category here";
    String filepath = "your link here";
    int harga = 0;
    int maxHarga = 0;

    //progressdialog
    private ProgressDialog progressDoalog;

    private ImageView loadRotateImage, backButton;
    private EditText descriptionAddDetail, amountAddDetail, dateAddDetail;
    private Button buttonSubmitDetail, buttonSubmitPCF, buttonSubmitGetPrice;
    private RecyclerView kategoriRecyclerView;

    // instance for firebase storage and StorageReference
    private FirebaseStorage storage;
    private StorageReference storageReference;

    //filepath
    private String path;

    //list for RV
    private List<Kategori> kategoriList = new ArrayList<Kategori>();
    private CategoryRecyclerAdapter categoryRecyclerAdapter;

    //pagination for category
    private int pageCategory = 0;
    private int maxPageCategory = 0;
    int maxItemCategory = 0;
    boolean isLoading = false;


    //permissioncodes
    int permissions_code = 42;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION
            , Manifest.permission.INTERNET};

    //locationstring
    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reimbursment_detail);
        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = this.getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);

        //startlocationupdates
        //init location
        initLocation();
        // restore the values from saved instance state
        restoreValuesFromBundle(savedInstanceState);
        startLocationButtonClick();

        //findview
        descriptionAddDetail = findViewById(R.id.description_add_re_detail);
        loadRotateImage = findViewById(R.id.showimagefromrotate);
        amountAddDetail = findViewById(R.id.amount_add_re_detail);
        buttonSubmitDetail = findViewById(R.id.submitbutton_add_re_detail);
        buttonSubmitGetPrice = findViewById(R.id.submitbutton_post_harga);
        buttonSubmitPCF = findViewById(R.id.submittopcfbutton_add_re_detail);
        backButton = findViewById(R.id.imageview_back_add_reimbursement_detail);
        kategoriRecyclerView = findViewById(R.id.recycler_kategori_add_reimbursement);
        dateAddDetail = findViewById(R.id.date_add_re_detail);

        //EDITTEXT LISTENER
        amountAddDetail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1 && s.toString().startsWith("0")) {
                    s.clear();
                }
            }
        });

        //setdate
        Date date = java.util.Calendar.getInstance().getTime();
        dateAddDetail.setText(date.toString());

        //SETUP CATEGORY RECYCLERVIEW
        kategoriRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        kategoriRecyclerView.setHasFixedSize(true);
        callEnqueKategoriAPI();
        categoryRecyclerAdapter = new CategoryRecyclerAdapter(this, this);
        categoryRecyclerAdapter.setMenuList(kategoriList);
        kategoriRecyclerView.setAdapter(categoryRecyclerAdapter);

        initScrollListener();

        // get the Firebase  storage reference
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        Bundle extras = getIntent().getExtras();
        path = extras.getString("picture");
        String ocr = extras.getString("description");
        Toast.makeText(this, "OCR TEXTS : " + ocr, Toast.LENGTH_SHORT).show();

        descriptionAddDetail.setText(ocr);
        //galleryAddPic(path);
        setPic(path);

        //upload image on start
        uploadImage();


        //onclick listeners
        buttonSubmitDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

        buttonSubmitGetPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AddReimbursmentDetailActivity.this, "" + filepath, Toast.LENGTH_SHORT).show();

                if (filepath != null) {
                    callEnqueOCRAPI(filepath);
                } else {
                    Toast.makeText(AddReimbursmentDetailActivity.this, "FILEPATH IS NULL!!"
                            , Toast.LENGTH_SHORT).show();
                }

                getDistanceAgitoFuse();

            }
        });

        buttonSubmitPCF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get both locations
                //TAKE 1st and 2nd coordinate

                stopLocationButtonClick();
                showLastKnownLocation();

                SharedPreferences prefs = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
                String coorfirst = prefs.getString("coordinatestart", "empty");
                String coorsecond = prefs.getString("coordinateend", "empty");
                boolean coortrue = true;
                Log.d("KEYREIMBURSE", "loadInitial: " + key);


                if (categoryRecyclerAdapter.getSelected() == null) {
                    Toast.makeText(AddReimbursmentDetailActivity.this
                            , "Please select your category!"
                            , Toast.LENGTH_SHORT).show();
                } else {
                    // set category
                    kategoryString = categoryRecyclerAdapter.getSelected();
                    maxHarga = categoryRecyclerAdapter.getMaxHarga();

                    //set harga
                    harga = (int) Long.parseLong(amountAddDetail.getText().toString());

                    //stop and get last location
                    stopLocationButtonClick();
                    showLastKnownLocation();

                    //continue checking
                    if (categoryRecyclerAdapter.getMaxHarga() < harga) {
                        Toast.makeText(AddReimbursmentDetailActivity.this
                                , "Your reimbursement is over the limit of reimbursement category," +
                                        "reimbursement amount will be set automatically to max value"
                                , Toast.LENGTH_SHORT)
                                .show();
                        harga = maxHarga;

                        if (filepath != null) {
                            Log.d("datasend", "onClick: " + kategoryString);
                            Log.d("datasend", "onClick: " + harga);
                            Log.d("datasend", "onClick: " + ocr);
                            Log.d("datasend", "onClick: " + filepath);
                            String desc = descriptionAddDetail.getText().toString();
                            ReimbursementBodyPost addnew = new ReimbursementBodyPost(kategoryString, (float) harga, desc, filepath, coorfirst, coorsecond, coortrue);
                            callEnquePostReimbursementAPI(addnew);
                        } else {
                            Toast.makeText(AddReimbursmentDetailActivity.this
                                    , "Picture hasn't been uploaded yet!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (filepath != null) {
                            Log.d("datasend", "onClick: " + kategoryString);
                            Log.d("datasend", "onClick: " + harga);
                            Log.d("datasend", "onClick: " + ocr);
                            Log.d("datasend", "onClick: " + filepath);

                            String desc = descriptionAddDetail.getText().toString();
                            ReimbursementBodyPost addnew = new ReimbursementBodyPost(kategoryString
                                    , (float) harga
                                    , desc
                                    , filepath
                                    , coorfirst
                                    , coorsecond
                                    , coortrue);
                            callEnquePostReimbursementAPI(addnew);
                        } else {
                            Toast.makeText(AddReimbursmentDetailActivity.this
                                    , "Picture hasn't been uploaded yet!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void galleryAddPic( String n) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(n);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void setPic(String n) {

        // Get the dimensions of the View

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        int height = 800;

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int ratio = photoH / height;
        int width = photoW * ratio;


        Log.d("UKURAN", "setPic: " + photoH + " , " + photoW);

        // Determine how much to scale down the image
        int scaleFactor = Math.min(width, height);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(n, bmOptions);
        loadRotateImage.setImageBitmap(bitmap);
    }

    // UploadImage method
    private void uploadImage()
    {
        if (path != null) {

            // Code for showing progressDialog while uploading
            ProgressDialog progressDialog
                    = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            // Defining the child of storageReference
            Date currentTime = Calendar.getInstance().getTime();
            StorageReference ref
                    = storageReference
                    .child(UUID.randomUUID().toString() + currentTime);

            // adding listeners on upload
            // or failure of image
            ref.putFile(Uri.fromFile(new File(path)))
                    .addOnSuccessListener(
                            new OnSuccessListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onSuccess(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {

                                    // Image uploaded successfully
                                    // Dismiss dialog
                                    progressDialog.dismiss();
                                    Toast.makeText(AddReimbursmentDetailActivity.this,
                                                    "Image Uploaded!!",
                                                    Toast.LENGTH_SHORT).show();

                                    ref.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            String profileImageUrl=task.getResult().toString();
                                            Log.d("URL llalalala",profileImageUrl);
                                            filepath = profileImageUrl;
                                            callEnqueOCRAPI(filepath);
                                        }
                                    });
                                    //finish
                                    //finish();
                                }
                            })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {

                            // Error, Image not uploaded
                            progressDialog.dismiss();
                            Toast
                                    .makeText(AddReimbursmentDetailActivity.this,
                                            "Failed " + e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                    .show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                // Progress Listener for loading
                                // percentage on the dialog box
                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {
                                    double progress
                                            = (100.0
                                            * taskSnapshot.getBytesTransferred()
                                            / taskSnapshot.getTotalByteCount());
                                    progressDialog.setMessage(
                                            "Uploaded "
                                                    + (int)progress + "%");
                                }
                            });
        }
    }

    private void callEnquePostReimbursementAPI(ReimbursementBodyPost x) {
        progressDoalog = new ProgressDialog(AddReimbursmentDetailActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .addNewReimbursement("Bearer " + key, x)
                .enqueue(new Callback<Reimbursement>() {
                    @Override
                    public void onResponse(Call<Reimbursement> call, Response<Reimbursement> response) {
                        progressDoalog.dismiss();
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());
                        if (response.isSuccessful()) {
                            // Do awesome stuff
                            Toast.makeText(AddReimbursmentDetailActivity.this,
                                    "Reimbursement Uploaded!",
                                    Toast.LENGTH_SHORT).show();

                            //show alert dialog
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddReimbursmentDetailActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("Submitted!");
                            builder.setMessage("Your reimbursement request has been submitted, you'll be redirected to homepage");
                            builder.setPositiveButton("Confirm",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });


                            AlertDialog dialog = builder.create();
                            dialog.show();


                        } else if (response.code() == 401) {
                            // Handle unauthorized
                            Toast.makeText(AddReimbursmentDetailActivity.this,
                                    "Unauthorized access! your reimbursement hasn't been uploaded",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Handle other responses

//                            String rererere = new Gson().toJson(response);
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String rere = jObjError.get("message").toString();

                                Toast.makeText(AddReimbursmentDetailActivity.this, rere, Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                Toast.makeText(AddReimbursmentDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            Toast.makeText(AddReimbursmentDetailActivity.this,
                                    "Unknown error! your reimbursement hasn't been uploaded : "
                                    ,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Reimbursement> call, Throwable t) {
                        Log.d("ARDA Kategori", "onFailure: " + t);
                        Toast.makeText(AddReimbursmentDetailActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callEnqueKategoriAPI() {

        progressDoalog = new ProgressDialog(AddReimbursmentDetailActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitResoClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getDaftarKategori("Bearer " + key, pageCategory, "reimburse")
                .enqueue(new Callback<KategoriServerResponse>() {
                    @Override
                    public void onResponse(Call<KategoriServerResponse> call, Response<KategoriServerResponse> response) {
                        //Log.d("getrawkategori", "onResponse: " + response.body());

                        progressDoalog.dismiss();

                        if (response.body() != null) {

                            try {
                                Log.d("get raw", "getraw: " + response.raw());
                                Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                        response.body().getOutputSchema().getCategories().getContent());
                                kategoriList.addAll(response.body().getOutputSchema().getCategories().getContent());
                                categoryRecyclerAdapter.notifyDataSetChanged();

                                maxPageCategory = response.body().getOutputSchema().getCategories().getTotal_pages();
                                maxItemCategory = response.body().getOutputSchema().getCategories().getTotal_elements();

                                amountAddDetail.setFocusable(true);
                                maxHarga = categoryRecyclerAdapter.getMaxHarga();
                                String maxAmountCat = String.valueOf(maxHarga);
                                amountAddDetail.setHint("");
                                amountAddDetail.setFilters(new InputFilter[]{ new InputFilterMinMax("1",maxAmountCat)});

                                if (pageCategory < maxPageCategory - 1) {
                                    pageCategory = pageCategory + 1;
                                }
                            }catch (Exception e) {
                                Log.d("ARDA Kategori"
                                        , "onResponse: DATA FAILURE" +
                                        e);
                                Toast.makeText(AddReimbursmentDetailActivity.this, "" + e
                                        , Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<KategoriServerResponse> call, Throwable t) {
                        Log.d("ARDA Kategori", "onFailure: " + t);
                        Toast.makeText(AddReimbursmentDetailActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callEnqueOCRAPI(String link) {
        progressDoalog = new ProgressDialog(AddReimbursmentDetailActivity.this);
        progressDoalog.setMessage("Loading your price....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        GetOCRPriceBodyRequest getPrice = new GetOCRPriceBodyRequest(link);

        RetrofitOCRClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .postOCRHarga(getPrice)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        progressDoalog.dismiss();
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());

                        try {
                            //Log.d("TAG get harga", "onResponse: " + response.body().string());
                            assert response.body() != null;
                            harga = Integer.parseInt(response.body().string());
                            amountAddDetail.setText(String.valueOf(harga));

                            Toast.makeText(AddReimbursmentDetailActivity.this, "" + harga
                                    , Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Log.d("TAG get harga", "onResponse: " + response.raw().toString());
                            Log.d("TAG get error : ", "error response: " + e);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("ARDA OCR", "onFailure: " + t);
                        Toast.makeText(AddReimbursmentDetailActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initScrollListener() {
        kategoriRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == kategoriList.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void loadMore() {
        kategoriList.add(null);
        categoryRecyclerAdapter.notifyItemInserted(kategoriList.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                kategoriList.remove(kategoriList.size() - 1);
                int scrollPosition = kategoriList.size();
                categoryRecyclerAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                if (maxItemCategory < nextLimit) {
                    nextLimit = maxItemCategory;
                }

                //addmore

                if(currentSize < nextLimit) {
                    callEnqueKategoriAPI();
                }

                categoryRecyclerAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);


    }

    @Override
    public void onListItemClicked(Kategori t) {
        //DO NOT USE THIS THING FOR ON CLICK, GO ON THE ADAPTOR
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //LOCATION RELATED
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this
                            , "Permission denied to read your External storage/" +
                                    "your internet / " +
                                    "your GPS", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("satisfiedsettings", "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("notsatisfied", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(AddReimbursmentDetailActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("unable to exec", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("error", errorMessage);

                                Toast.makeText(AddReimbursmentDetailActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void stopLocationButtonClick() {
        mRequestingLocationUpdates = false;
        stopLocationUpdates();
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void showLastKnownLocation() {
        if (mCurrentLocation != null) {
            Log.d("LOCATION", "showLastKnownLocation on add reimbursement: not null"  );
            Toast.makeText(getApplicationContext(), "Lat: " + mCurrentLocation.getLatitude()
                    + ", Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG).show();

            //save required data
            SharedPreferences sharedPrefss = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPrefss.edit();

            String coor1 = mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
            editor.putString("coordinateend", coor1);
            editor.putLong("timestampend", System.currentTimeMillis()/1000);
            editor.commit();


            Log.d("COORDINATE", "showLastKnownLocation: coor 2 : " + coor1);
            Log.d("COORDINATE", "showLastKnownLocation: TIME 2 : " + System.currentTimeMillis()/1000);
        } else {
            Log.d("LOCATION", "showLastKnownLocation on add reimbursement: failed"  );
            Toast.makeText(getApplicationContext(), "Last known location is not available!", Toast.LENGTH_SHORT).show();
        }
    }

    //-------------------------- CALCULATION DISTANCE FUNCTION ---------------------------
    private void getDistanceAgitoFuse() {
        //stop location
        stopLocationButtonClick();

        //get last location
        showLastKnownLocation();

        //get both values
        SharedPreferences prefs = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        String coorfirst = prefs.getString("coordinatestart", "empty");
        String coorsecond = prefs.getString("coordinateend", "empty");
        Long timeStampFirst = prefs.getLong("timestampstart", 0);
        Long timeStampEnd = prefs.getLong("timestampend", 0);

        String coorFirstList[] = coorfirst.split(",");
        double latFirst = Double.parseDouble(coorFirstList[0]);
        double longFirst = Double.parseDouble(coorFirstList[1]);

        String coorSecondList[] = coorsecond.split(",");
        double latSecond = Double.parseDouble(coorSecondList[0]);
        double longSecond = Double.parseDouble(coorSecondList[1]);

        //calculate both distance
        double distanceCounted = simplerThanHaversine(latFirst, longFirst, latSecond, longSecond);
        Toast.makeText(this, "location counted : " + distanceCounted + "km", Toast.LENGTH_SHORT).show();

        //calculate time :
        double timeCounted = longSecond - longFirst;

        //LOG
        Log.d("COORDINATE", "getDistanceAgitoFuse: LAT 1 : " + latFirst);
        Log.d("COORDINATE", "getDistanceAgitoFuse: LAT 2 : " + latSecond);
        Log.d("COORDINATE", "getDistanceAgitoFuse: Long 1 : " + longFirst);
        Log.d("COORDINATE", "getDistanceAgitoFuse: long 2 : " + longSecond);
        Log.d("COORDINATE", "getDistanceAgitoFuse: timestamp 1 : " + timeStampFirst);
        Log.d("COORDINATE", "getDistanceAgitoFuse: timestamp 2 : " + timeStampEnd);
        Log.d("COORDINATE", "getDistanceAgitoFuse: Time : " + timeCounted + " ms");
        Log.d("COORDINATE", "getDistanceAgitoFuse: Distance : " + distanceCounted + " meter");

    }

    static double simplerThanHaversine(double lat1, double lon1,
                            double lat2, double lon2)
    {
        Location me   = new Location("");
        Location dest = new Location("");

        me.setLatitude(lat1);
        me.setLongitude(lon1);

        dest.setLatitude(lat2);
        dest.setLongitude(lon2);

        double dist = me.distanceTo(dest);
        return dist;
    }

    private Double deg2rad(Double deg) {
        return deg * (Math.PI/180);
    }

    private Boolean calculateSpeed(Double time, Double distance) {

        return true;
    }
    //========================= CALCULATION DISTANCE FUNCTION ==========================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e("Agree tag", "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e("disacgreetag", "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }
}