package com.example.projectmantap11.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projectmantap11.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ReimbursementDetailActivity extends AppCompatActivity {

    TextView nameTV, reimburseIDTV, invoiceIDTV, categoryTV, amountTV, noteTV, descriptionTV, submitDateTV;
    ImageView receiptPicture, backButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reimbursement_detail);

        //hide actionbar
        getSupportActionBar().hide();

        //findview;
        categoryTV = findViewById(R.id.textview_reimbursement_detail_category);
        amountTV = findViewById(R.id.textview_reimbursement_detail_amount);
        descriptionTV = findViewById(R.id.textview_reimbursement_detail_description);
        receiptPicture = findViewById(R.id.reimburse_detail_photo);
        reimburseIDTV = findViewById(R.id.reimburse_detail_id);
        submitDateTV = findViewById(R.id.textview_reimbursement_detail_submit_date);
        backButton = findViewById(R.id.reimbursement_detail_back_button);

        //get intent
        Intent intent = getIntent();
        String invoiceID = intent.getStringExtra("invoiceID");
        String category = intent.getStringExtra("category");
        String amount = intent.getStringExtra("amount");
        String description = intent.getStringExtra("description");
        String filepath = intent.getStringExtra("filepath");
        String submitDateString = intent.getStringExtra("submitdate");

        //settext
        categoryTV.setText(category);
        amountTV.setText(amount);
        descriptionTV.setText(description);
        reimburseIDTV.setText(invoiceID);
        submitDateTV.setText(submitDateString);

        //set image
        if (filepath.isEmpty()) {
            receiptPicture.setImageResource(R.drawable.logo_vertical);
        } else {
            Picasso.get().load(filepath).into(receiptPicture);
        }

        //set onclicks
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}