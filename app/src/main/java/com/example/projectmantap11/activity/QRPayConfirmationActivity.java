package com.example.projectmantap11.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.biometric.BiometricManager;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.projectmantap11.BuildConfig;
import com.example.projectmantap11.R;
import com.example.projectmantap11.adapter.CategoryRecyclerAdapter;
import com.example.projectmantap11.adapter.VirtualCardActiveAdapter;
import com.example.projectmantap11.helper.InputFilterMinMax;
import com.example.projectmantap11.models.Authorization;
import com.example.projectmantap11.models.QRTransactionSend;
import com.example.projectmantap11.models.QRTransactionServerResponse;
import com.example.projectmantap11.models.ReimbursementServerResponse;
import com.example.projectmantap11.models.VCListServerResponse;
import com.example.projectmantap11.models.VirtualCard;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.example.projectmantap11.services.RetrofitTransactionClientInstance;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class QRPayConfirmationActivity extends AppCompatActivity
        implements VirtualCardActiveAdapter.ListItemClickListener{

    TextView namaMerchantTV, hargaMerchantTV, virtualCardChoosenTV;
    Button confirmQRBtn, cancelQRBtn;
    RecyclerView cardChoserRV;
    PinView qrPayInputPIN;

    //QR strings
    private String namaMerchant, nomorMerchant, hargaMerchant;

    //VC LIst
    private List<VirtualCard> listOfVCPaying;
    private VirtualCardActiveAdapter virtualCardActiveAdapter;

    //pagination for virtualcard active
    private int pageVirtualCardActive = 0;
    private int maxPageVirtualCardActive = 0;
    int maxItemVirtualCardActive = 0;
    boolean isLoading = false;

    //progressdialog
    private ProgressDialog progressDoalog;

    //your key here
    String key = "your key here";

    //submit data
    String carddate = "your card date here";

    Double harga = 0.0;
    int maxHarga = 0;

    //COORDINATE RELATED
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    //permissioncodes
    int permissions_code = 42;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION
            , Manifest.permission.INTERNET};

    //locationstring
    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_pay_confirmation);

        getSupportActionBar().hide();

        //TAKE KEY
        SharedPreferences prefs = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);


        //START LOCATION SERVICES
        //init location
        initLocation();
        // restore the values from saved instance state
        restoreValuesFromBundle(savedInstanceState);

        //start location updates :
        startLocationButtonClick();

        //findview
        namaMerchantTV = findViewById(R.id.nama_merchant_confirmation);
        hargaMerchantTV = findViewById(R.id.harga_merchant_confirmation);
        confirmQRBtn = findViewById(R.id.confirm_qr_payment_button);
        cancelQRBtn = findViewById(R.id.cancel_qr_payment_button);
        cardChoserRV = findViewById(R.id.card_choser_rv);
        qrPayInputPIN = findViewById(R.id.qr_payment_pin_input);

        //get needed strings
        Intent intent = getIntent();
        namaMerchant = intent.getStringExtra("namamerchant");
        nomorMerchant = intent.getStringExtra("nomormerchant");
        hargaMerchant = "Rp " + intent.getStringExtra("hargamerchant");
        harga = Double.parseDouble(Objects.requireNonNull(intent.getStringExtra("hargamerchant")));

        Locale indo = new Locale("id", "ID");
        NumberFormat indoFormat = NumberFormat.getCurrencyInstance(indo);
        String hargaShow = String.valueOf(indoFormat.format(harga));

        if (namaMerchant != null
                && nomorMerchant != null
                && intent.getStringExtra("hargamerchant") != null) {
            namaMerchantTV.setText(namaMerchant);
            hargaMerchantTV.setText(hargaShow);
        }

        //INITIATE VC LIST
        listOfVCPaying = new ArrayList<>();

        //SETUP VC ACTIVE RV
        cardChoserRV.setLayoutManager(new LinearLayoutManager(this));
        cardChoserRV.setHasFixedSize(true);
        virtualCardActiveAdapter = new VirtualCardActiveAdapter(this, this);
        virtualCardActiveAdapter.setMenuList(listOfVCPaying);
        cardChoserRV.setAdapter(virtualCardActiveAdapter);
        hitGetAllVC();
        initScrollListener();

        //setonclick
        //confirmbutton
        confirmQRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(QRPayConfirmationActivity.this, QRFinishActivity.class);
//                startActivity(intent);
//                finish();

                hitQRPayAPI();
            }
        });

        //cancelbutton
        cancelQRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }



    //pay API void
    private void hitQRPayAPI() {
        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(QRPayConfirmationActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);
        stopLocationUpdates();

        String cardNumber = virtualCardActiveAdapter.getCardNumberSelected();
        String cardDate = virtualCardActiveAdapter.getCardDateSelected();
        String cardDateFormattedList[] = cardDate.split("T");
        String cardDateFormatted = cardDateFormattedList[0];

        String cardDateFormattedList2[] = cardDateFormatted.split("-");

        String cardDateFormatted2 = cardDateFormattedList2[1] + "/" +
                cardDateFormattedList2[0];

        String pin = Objects.requireNonNull(qrPayInputPIN.getText()).toString();
        String merchant = namaMerchant;
        String accountNumber = nomorMerchant;
        double amount = harga;

        //GET last and first location
        stopLocationButtonClick();
        showLastKnownLocation();
        SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        String coordinateStart = sharedPref.getString("coordinatestart", "empty");
        String coordinateEnd = sharedPref.getString("coordinateend", "empty");


        QRTransactionSend a = new QRTransactionSend(
                cardNumber,
                cardDateFormatted2,
                pin,
                merchant,
                accountNumber,
                amount,
                coordinateStart,
                coordinateEnd,
                true);

        //Start hitting the QRPay API
        RetrofitTransactionClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .bayarPakeQR("Bearer " + key, a)
                .enqueue(new Callback<QRTransactionServerResponse>() {
                    @Override
                    public void onResponse(Call<QRTransactionServerResponse> call, Response<QRTransactionServerResponse> response) {
                        Log.d("REIMBURSERESPONSE", "onResponse: " + response.raw().toString());

                        progressDoalog.dismiss();

                        if (response.body() != null) {

                            Log.d("QRPAY", "onResponse: response not null");
                            boolean transaction = response.body().getOutputSchema().getTransaction().isTransaction_success();

                            String responselalala = response.body().getError_schema().getError_message().getEnglish();

                            if (transaction) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(QRPayConfirmationActivity.this);
                                builder.setCancelable(true);
                                builder.setTitle("Submitted!");
                                builder.setMessage("Payment Success!, you'll be redirected to homepage");
                                builder.setPositiveButton("Confirm",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        });


                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } else if (responselalala.equals("Transaction failed")){
                                progressDoalog.dismiss();
                                Toast.makeText(QRPayConfirmationActivity.this
                                        , "Your transaction failed, please try again"
                                        , Toast.LENGTH_SHORT).show();
                                startLocationUpdates();
                            } else {
                                progressDoalog.dismiss();
                                Toast.makeText(QRPayConfirmationActivity.this
                                        , "An error occurred, please try again later"
                                        , Toast.LENGTH_SHORT).show();
                                startLocationUpdates();
                            }
                        } else {
                            Log.d("QRPAY", "onResponse: response null");
                            AlertDialog.Builder builder = new AlertDialog.Builder(QRPayConfirmationActivity.this);
                            builder.setCancelable(true);
                            builder.setTitle("Failed!");
                            builder.setMessage("Payment failed! Please check your PIN and/or try again later");
                            builder.setPositiveButton("Confirm",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });


                            AlertDialog dialog = builder.create();
                            dialog.show();
                            startLocationUpdates();
                        }
                    }

                    @Override
                    public void onFailure(Call<QRTransactionServerResponse> call, Throwable t) {
                        Log.d("REIMBURSEOnFailure", "onFailure: " + t);
                        startLocationUpdates();

                    }
                });
    }

    //pay API void
    private void hitGetAllVC() {
        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(QRPayConfirmationActivity.this);
        progressDoalog.setMessage("Getting your virtual cards...");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        //Start hitting the VC List API
        RetrofitTransactionClientInstance
                .getRetrofit()
                .create(ApiService.class)
                .getDaftarActiveVC("Bearer " + key, true)
                .enqueue(new Callback<VCListServerResponse>() {
                    @Override
                    public void onResponse(Call<VCListServerResponse> call, Response<VCListServerResponse> response) {
                        Log.d("VCACTIVELIST", "onResponse: " + response.raw().toString());

                        if (response.body() != null) {
                            progressDoalog.dismiss();

                            Log.d("get raw", "getraw: " + response.raw());
                            Log.d("TestPlaneKategori", "onResponse: GET DATA : "+
                                    response.body().getOutput_schema().getVirtual_cards().getContent());
                            listOfVCPaying.addAll(response.body().getOutput_schema().getVirtual_cards().getContent());
                            virtualCardActiveAdapter.notifyDataSetChanged();

                            maxPageVirtualCardActive = response.body().getOutput_schema().getVirtual_cards().getTotal_pages();
                            maxItemVirtualCardActive = response.body().getOutput_schema().getVirtual_cards().getTotal_elements();


                            if (pageVirtualCardActive < maxPageVirtualCardActive - 1) {
                                pageVirtualCardActive = pageVirtualCardActive + 1;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VCListServerResponse> call, Throwable t) {
                        Log.d("REIMBURSEOnFailure", "onFailure: " + t);
                        listOfVCPaying.add(null);
                        progressDoalog.dismiss();
                        Toast.makeText(QRPayConfirmationActivity.this, "" + t
                                , Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initScrollListener() {
        cardChoserRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition()
                            == listOfVCPaying.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void loadMore() {
        listOfVCPaying.add(null);
        virtualCardActiveAdapter.notifyItemInserted(listOfVCPaying.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listOfVCPaying.remove(listOfVCPaying.size() - 1);
                int scrollPosition = listOfVCPaying.size();
                virtualCardActiveAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                if (maxItemVirtualCardActive < nextLimit) {
                    nextLimit = maxItemVirtualCardActive;
                }

                //addmore

                if(currentSize < nextLimit) {
                    hitGetAllVC();
                }

                virtualCardActiveAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);


    }

    //----------------------- FOR COORDINATE -------------------------------


    //LOCATION RELATED
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this
                            , "Permission denied to read your External storage/" +
                                    "your internet / " +
                                    "your GPS", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("satisfiedsettings", "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!"
                                , Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("notsatisfied", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(QRPayConfirmationActivity.this
                                            , REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("unable to exec", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("error", errorMessage);

                                Toast.makeText(QRPayConfirmationActivity.this, errorMessage
                                        , Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    public void stopLocationButtonClick() {
        mRequestingLocationUpdates = false;
        stopLocationUpdates();
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void showLastKnownLocation() {
        if (mCurrentLocation != null) {
            Toast.makeText(getApplicationContext(), "Lat: " + mCurrentLocation.getLatitude()
                    + ", Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG).show();

            //save required data
            SharedPreferences sharedPref = getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            String coor1 = mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();
            editor.putString("coordinateend", coor1);
            editor.apply();
        } else {
            Toast.makeText(getApplicationContext(), "Last known location is not available!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e("Agree tag", "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e("disacgreetag", "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }

    //======================= FOR COORDINATE ===============================


    @Override
    public void onListItemClicked(VirtualCard t) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


}