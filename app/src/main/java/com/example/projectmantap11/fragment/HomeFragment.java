package com.example.projectmantap11.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.activity.ChangePasswordActivity;
import com.example.projectmantap11.activity.HomeActivity;
import com.example.projectmantap11.activity.LoginActivity;
import com.example.projectmantap11.activity.NewVCRRequestActivity;
import com.example.projectmantap11.activity.NotificationActivity;
import com.example.projectmantap11.activity.ScanQRActivity;
import com.example.projectmantap11.activity.VirtualCardDetailsActivity;
import com.example.projectmantap11.activity.VirtualCardListActivity;
import com.example.projectmantap11.models.CompanyServerResponse;
import com.example.projectmantap11.models.VCRequestListServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    //views
    private ShimmerFrameLayout mShimmerViewContainer;
    private ImageView newReimbursement, historyReimbursement, vCardDetail, vCardPay, notificationButton
            ,requestNewVCIV, companyLogoIV;
    private TextView viewname, dateTV, locationTV, companyNameTV;

    //location
    private FusedLocationProviderClient fusedLocationClient;

    //permissions
    //permissioncodes
    int permissions_code = 42;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION
            , Manifest.permission.INTERNET};

    //isenqueing
    private boolean isEnqueing = false;

    //KEY
    private String key = "empty";

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //TAKE KEY
        SharedPreferences prefs = getActivity().getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYCHANGEPASS", "loadInitial: " + key);

        //findview
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        dateTV = view.findViewById(R.id.dateHome);
        vCardDetail = view.findViewById(R.id.home_vcdetail_iv);
        viewname = view.findViewById(R.id.userHome);
        vCardPay = view.findViewById(R.id.vcardpaywithqr);
        notificationButton = view.findViewById(R.id.notificationbutton);
        locationTV = view.findViewById(R.id.home_location_tv);
        requestNewVCIV = view.findViewById(R.id.home_request_new_vc_iv);
        companyNameTV = view.findViewById(R.id.home_company_name_tv);
        companyLogoIV = view.findViewById(R.id.home_company_logo_iv);

        //get and set name
        SharedPreferences sharedPref = getActivity().getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        String yourname = sharedPref.getString("employeename", "empty");
        viewname.setText("Hi, " + yourname);

        //stop shimmer + hide no card
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.hideShimmer();

        //set date
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy");
        String dateString = sdf.format(date);
        dateTV.setText(dateString);

        //set reimbursement menu
        newReimbursement = view.findViewById(R.id.addnewreimbursement);
        historyReimbursement = view.findViewById(R.id.showreimbursementhistory);

        //location shower
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity()
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    permissions,
                    permissions_code);
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            /*------- To get city name from coordinates -------- */
                            new Thread(new Runnable() {
                                public void run() {
                                    // a potentially time consuming task
                                    Geocoder geocoder = new Geocoder(getActivity().getBaseContext(), Locale.getDefault());
                                    Address address;
                                    List<Address> list;
                                    try {
                                        list = geocoder.getFromLocation(location.getLatitude(),location.getLongitude()
                                                ,1);//33.64600, 72.96115

                                        address = list.get(0);
                                        String cityname =  address.getLocality();
                                        String countryname =   address.getCountryName();
                                        Log.d("cityname", cityname);
                                        Log.d("countryname", countryname);
                                        locationTV.setText(cityname + ", " + countryname);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    }
                });

        //onclick listeners
        notificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        newReimbursement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomSheetFragment().show(getActivity().getSupportFragmentManager(), "Dialog");
            }
        });

        historyReimbursement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navView.setSelectedItemId(R.id.navigation_dashboard);
            }
        });

        requestNewVCIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewVCRRequestActivity.class);
                startActivity(intent);
            }
        });

        vCardDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VirtualCardListActivity.class);
                startActivity(intent);
            }
        });

        vCardPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ScanQRActivity.class);
                startActivity(intent);
            }
        });

        //CALL FOR COMPANY NAME AND LOGO
        callEnqueMyCompanyAPI();

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity()
                            , "Permission denied to read your External storage/" +
                                    "your internet / " +
                                    "your GPS", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void callEnqueMyCompanyAPI() {

        ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        if (!isEnqueing) {
            isEnqueing = true;
            RetrofitClientInstance
                    .getRetrofitInstance()
                    .create(ApiService.class)
                    .getCompanyData("Bearer " + key)
                    .enqueue(new Callback<CompanyServerResponse>() {
                        @Override
                        public void onResponse(Call<CompanyServerResponse> call, Response<CompanyServerResponse> response) {
                            progressDoalog.dismiss();
                            isEnqueing = false;
                            Log.d("COMPANYDATA", "onResponse: " + response.raw().toString());

                            if (response.body() != null) {

                                try {
                                    String companyName = response.body().getOutput_schema().getCompany().getName();
                                    String companyLogoPath = response.body().getOutput_schema().getCompany().getLogo();

                                    companyNameTV.setText(companyName);
                                    Picasso.get().load(companyLogoPath).into(companyLogoIV);
                                }catch (Exception e) {
                                    Log.d("COMPANYDATA", "onResponse: DATA FAILURE" +
                                            e);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CompanyServerResponse> call, Throwable t) {
                            progressDoalog.dismiss();
                            Log.d("COMPANYDATA", "onFailure: " + t);
                            isEnqueing = false;

                        }
                    });
        } else {
            //LOGGING
            Log.d("VCRPAGING", "callEnqueAPI: HIT PAGE still hitting, don't mess up the RV please");
        }
    }

}