package com.example.projectmantap11.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.activity.ReimbursementDetailActivity;
import com.example.projectmantap11.activity.SortingActivity;
import com.example.projectmantap11.adapter.ReimburseRecyclerAdapter;
import com.example.projectmantap11.adapter.VirtualCardRequestRecyclerAdapter;
import com.example.projectmantap11.models.Reimbursement;
import com.example.projectmantap11.models.ReimbursementServerResponse;
import com.example.projectmantap11.models.VCRequestListServerResponse;
import com.example.projectmantap11.models.VirtualCard;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitResoClientInstance;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class HistoryFragment extends Fragment
        implements ReimburseRecyclerAdapter.ListItemClickListener
        , VirtualCardRequestRecyclerAdapter.ListItemClickListener {

    private RecyclerView historyRecycler;

    //LIST FOR REIMBURSEMENT
    private ArrayList<Reimbursement> reimbursementList;
    private boolean isLoading = false;
    private int page = 0;
    private int maxPage = 0;
    int maxItem = 0;
    private boolean isEnqueing = false;

    //string for sorting
    private String reimbursementCategory = null;
    private String reimbursementStatus = null;
    private String reimbursementOrder = null;
    private String reimbursementDirection = null;

    //LIST FOR VIRTUALCARD
    private ArrayList<VirtualCard> virtualCardList;

    private ShimmerFrameLayout mShimmerViewContainer;

    //normal RV
    ReimburseRecyclerAdapter mAdapter;
    VirtualCardRequestRecyclerAdapter vcAdapter;

    //your key here
    String key = "your key here";

    //switcher between virtual card and reimbursement
    private Button virtualCardSwitcher, reimbursementSwitcher;
    private boolean isReimbursement = true;

    //sorting FAB
    private FloatingActionButton sortingFAB;

    //REQUEST CODES
    private int REQUEST_CODE_SORTING = 101;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        //TAKE KEY
        SharedPreferences prefs = getActivity().getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);

        //findview
        reimbursementSwitcher = view.findViewById(R.id.switcher_reimbursement_button);
        virtualCardSwitcher = view.findViewById(R.id.switcher_virtualcard_button);
        sortingFAB = view.findViewById(R.id.sorting_fab);
        historyRecycler = view.findViewById(R.id.recycler_history);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_history_container);


        //onclicks
        reimbursementSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isReimbursement = true;
                buttonSwitcher();
            }
        });

        virtualCardSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isReimbursement = false;
                buttonSwitcher();
            }
        });

        sortingFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getActivity(), SortingActivity.class);
                intent.putExtra("isreimbursement", isReimbursement);
                startActivityForResult(intent, REQUEST_CODE_SORTING);
            }
        });

        //run first switch
        buttonSwitcher();


        mShimmerViewContainer.startShimmer();

        return view;
    }

    private void setupReimbursementRecyclerview() {
        reimbursementList = new ArrayList<Reimbursement>();
        callEnqueAPI();
        mAdapter = new ReimburseRecyclerAdapter(getActivity(),this );
        mAdapter.setMenuList(reimbursementList);
        historyRecycler.setAdapter(mAdapter);
        initScrollListener();
    }

    private void setupVCRRecyclerView() {
        virtualCardList = new ArrayList<VirtualCard>();
        callEnqueVCRAPI();
        vcAdapter = new VirtualCardRequestRecyclerAdapter(getActivity(), this);
        vcAdapter.setMenuList(virtualCardList);
        historyRecycler.setAdapter(vcAdapter);
        initScrollListenerVirtualCard();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_SORTING) {
            if (data.hasExtra("categoryfilter")) {
                Log.d("SORTING", "onActivityResult: input kategory : " + data.getStringExtra("categoryfilter"));
                reimbursementCategory = data.getStringExtra("categoryfilter");
            }  else {
                reimbursementCategory = null;
            }

            if (data.hasExtra("statusfilter")) {
                Log.d("SORTING", "onActivityResult: input status : " + data.getStringExtra("statusfilter"));
                reimbursementStatus = data.getStringExtra("statusfilter");
            } else {
                reimbursementStatus = null;
            }

            if (data.hasExtra("orderfilter")) {
                Log.d("SORTING", "onActivityResult: input order : " + data.getStringExtra("orderfilter"));
                reimbursementOrder = data.getStringExtra("orderfilter");
            } else {
                reimbursementOrder = null;
            }

            if (data.hasExtra("directionfilter")) {
                Log.d("SORTING", "onActivityResult: input direction : " + data.getStringExtra("directionfilter"));
                reimbursementDirection = data.getStringExtra("directionfilter");
            } else {
                reimbursementDirection = null;
            }
        } else {
            reimbursementStatus = null;
            reimbursementCategory = null;
            reimbursementDirection = null;
            reimbursementOrder = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //setup recyclerview
        historyRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyRecycler.setHasFixedSize(true);

        if (isReimbursement) {
            page = 0;
            maxPage = 0;
            maxItem = 0;

            setupReimbursementRecyclerview();
        } else {
            page = 0;
            maxPage = 0;
            maxItem = 0;

            setupVCRRecyclerView();
        }
    }

    //--------------POPULATE DATA -------------------
    private void callEnqueAPI() {

        //LOGGING
        Log.d("REIMBURSEPAGING", "callEnqueAPI: HIT PAGE " + page);

        if (!isEnqueing) {
            isEnqueing = true;
            RetrofitResoClientInstance
                    .getRetrofit()
                    .create(ApiService.class)
                    .getDaftarAllReimbursement("Bearer " + key, 0, page, reimbursementCategory, reimbursementStatus, reimbursementOrder, reimbursementDirection)
                    .enqueue(new Callback<ReimbursementServerResponse>() {
                        @Override
                        public void onResponse(Call<ReimbursementServerResponse> call, Response<ReimbursementServerResponse> response) {
                            Log.d("REIMBURSEPAGING", "onResponse: " + response.raw().toString());

                            isEnqueing = false;

                            if (response.body() != null) {

                                Log.d("REIMBURSEPAGING", "onResponse: GET DATA : "+
                                        response.body().getOutput_schema().getReimburses().getContent());
                                reimbursementList.addAll(response.body().getOutput_schema().getReimburses().getContent());
                                mAdapter.notifyDataSetChanged();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                maxPage = response.body().getOutput_schema().getReimburses().getTotal_pages();
                                maxItem = response.body().getOutput_schema().getReimburses().getTotal_elements();
                                Log.d("REIMBURSEPAGING", "onResponse: MAX ITEM = "+
                                        maxItem);
                                Log.d("REIMBURSEPAGING", "onResponse: MAX PAGE = "+
                                        maxPage);
                                Log.d("REIMBURSEPAGING", "onResponse: current PAGE HIT = " + page
                                );

                                Log.d("REIMBURSEPAGING", "onResponse: current ITEM HIT = " + mAdapter.getItemCount()
                                );

                                if (page < maxPage - 1) {
                                    page = page + 1;
                                }

                                Log.d("REIMBURSEPAGING", "onResponse: next page hit " + page);
                            }
                        }

                        @Override
                        public void onFailure(Call<ReimbursementServerResponse> call, Throwable t) {
                            Log.d("REIMBURSEPAGING", "onFailure: " + t);
                            isEnqueing = false;

                        }
                    });
        } else {
            //LOGGING
            Log.d("REIMBURSEPAGING", "callEnqueAPI: HIT PAGE still hitting, don't mess up the RV please");
        }
    }

    private void callEnqueVCRAPI() {

        if (!isEnqueing) {
            isEnqueing = true;
            RetrofitResoClientInstance
                    .getRetrofit()
                    .create(ApiService.class)
                    .getRequestVirtualCard("Bearer " + key,page,reimbursementCategory,reimbursementStatus,reimbursementOrder,reimbursementDirection )
                    .enqueue(new Callback<VCRequestListServerResponse>() {
                        @Override
                        public void onResponse(Call<VCRequestListServerResponse> call, Response<VCRequestListServerResponse> response) {
                            isEnqueing = false;
                            Log.d("VCRPAGING", "onResponse: " + response.raw().toString());

                            if (response.body() != null) {

                                try {
                                    Log.d("VCRPAGING", "onResponse: GET DATA : "+
                                            response.body().getOutput_schema().getVirtual_card_requests().getContent());
                                    virtualCardList.addAll(response.body().getOutput_schema().getVirtual_card_requests().getContent());
                                    vcAdapter.notifyDataSetChanged();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                    maxPage = response.body().getOutput_schema().getVirtual_card_requests().getTotal_pages();
                                    maxItem = response.body().getOutput_schema().getVirtual_card_requests().getTotal_elements();
                                    Log.d("VCRPAGING", "onResponse: MAX ITEM = "+
                                            maxItem);

                                    if (page < maxPage - 1) {
                                        page = page + 1;
                                    }
                                }catch (Exception e) {
                                    Log.d("VCRPAGING", "onResponse: DATA FAILURE" +
                                            e);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<VCRequestListServerResponse> call, Throwable t) {
                            Log.d("VCRPAGING", "onFailure: " + t);
                            isEnqueing = false;

                        }
                    });
        } else {
            //LOGGING
            Log.d("VCRPAGING", "callEnqueAPI: HIT PAGE still hitting, don't mess up the RV please");
        }
    }
    //--------------POPULATE DATA -------------------

    private void initScrollListener() {
        historyRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0 && sortingFAB.getVisibility() == View.VISIBLE) {
                    sortingFAB.hide();
                } else if (dy < 0 && sortingFAB.getVisibility() != View.VISIBLE) {
                    sortingFAB.show();
                }

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (isReimbursement) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == reimbursementList.size() - 1) {
                            //bottom of list!
                            loadMore();
                            isLoading = true;
                        }
                    } else {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == virtualCardList.size() - 1) {
                            //bottom of list!
                            loadMoreVirtualCard();
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }

    private void loadMore() {
        reimbursementList.add(null);
        mAdapter.notifyItemInserted(reimbursementList.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                reimbursementList.remove(reimbursementList.size() - 1);
                int scrollPosition = reimbursementList.size();
                mAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                Log.d("REIMBURSEPAGING", "run: currentsize = "  + currentSize);
                Log.d("REIMBURSEPAGING", "run: maxitem = "  + maxItem);
                Log.d("REIMBURSEPAGING", "run: page = "  + page);

                Log.d("REIMBURSEPAGING", "run: scrollposition = "  + scrollPosition);

                if (currentSize > maxItem) {
                    currentSize = maxItem;
                }

                if (maxItem < nextLimit) {
                    nextLimit = maxItem;
                }

                //addmore

                if(currentSize < nextLimit) {
                    callEnqueAPI();
                }

                mAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);
    }

    //two code below for virtual card
    private void initScrollListenerVirtualCard() {
        historyRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0 && sortingFAB.getVisibility() == View.VISIBLE) {
                    sortingFAB.hide();
                } else if (dy < 0 && sortingFAB.getVisibility() != View.VISIBLE) {
                    sortingFAB.show();
                }

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == virtualCardList.size() - 1) {
                        //bottom of list!
                        loadMoreVirtualCard();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMoreVirtualCard() {
        virtualCardList.add(null);
        historyRecycler.post(new Runnable() {
            public void run() {
                vcAdapter.notifyItemInserted(virtualCardList.size() - 1);
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                virtualCardList.remove(virtualCardList.size() - 1);
                int scrollPosition = virtualCardList.size();
                vcAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                Log.d("loadmore", "run: currentsize = "  + currentSize);
                Log.d("loadmore", "run: maxitem = "  + maxItem);
                Log.d("loadmore", "run: page = "  + page);

                if (maxItem < nextLimit) {
                    nextLimit = maxItem;
                }

                //addmore

                if(currentSize < nextLimit) {
                    callEnqueVCRAPI();
                }

                vcAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 0);
    }

    private void buttonSwitcher() {
        if (!isReimbursement) {
            reimbursementSwitcher.setBackgroundResource(R.drawable.background_textbox);
            reimbursementSwitcher.setTextColor(Color.BLACK);
            virtualCardSwitcher.setBackgroundResource(R.drawable.background_statustext_blue);
            virtualCardSwitcher.setTextColor(Color.WHITE);

            maxItem = 0;
            maxPage = 0;
            page = 0;

            reimbursementCategory = null;
            reimbursementOrder = null;
            reimbursementDirection = null;
            reimbursementStatus = null;

            mShimmerViewContainer.setVisibility(View.VISIBLE);
            mShimmerViewContainer.startShimmer();
            setupVCRRecyclerView();

        } else {
            reimbursementSwitcher.setBackgroundResource(R.drawable.background_statustext_blue);
            reimbursementSwitcher.setTextColor(Color.WHITE);
            virtualCardSwitcher.setBackgroundResource(R.drawable.background_textbox);
            virtualCardSwitcher.setTextColor(Color.BLACK);

            maxItem = 0;
            maxPage = 0;
            page = 0;

            reimbursementCategory = null;
            reimbursementOrder = null;
            reimbursementDirection = null;
            reimbursementStatus = null;

            mShimmerViewContainer.setVisibility(View.VISIBLE);
            mShimmerViewContainer.startShimmer();
            setupReimbursementRecyclerview();

        }
    }

    @Override
    public void onListItemClicked(Reimbursement t) {

        String name = t.getName();
        String reimburseID = t.getId();
        String invoiceID = t.getInvoiceID();
        String category = t.getCategories();
        String amount = t.getUang();
        String note = t.getNote();
        String description = t.getDescription();
        String filepath = t.getImagePath();
        String submitDate = t.getCreatedDate();

        Intent intent = new Intent(getActivity(), ReimbursementDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("reimburseID", reimburseID);
        intent.putExtra("invoiceID", invoiceID);
        intent.putExtra("category", category);
        intent.putExtra("amount", amount);
        intent.putExtra("note", note);
        intent.putExtra("description", description);
        intent.putExtra("filepath", filepath);
        intent.putExtra("submitdate", submitDate);

        startActivity(intent);
    }

    @Override
    public void onListItemClicked(VirtualCard t) {

    }
}