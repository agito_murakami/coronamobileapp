package com.example.projectmantap11.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmantap11.R;
import com.example.projectmantap11.activity.LoginActivity;
import com.example.projectmantap11.activity.ProfileEditActivity;
import com.example.projectmantap11.models.KaryawanServerResponse;
import com.example.projectmantap11.models.VCRequestListServerResponse;
import com.example.projectmantap11.services.ApiService;
import com.example.projectmantap11.services.RetrofitClientInstance;
import com.example.projectmantap11.services.RetrofitResoClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    //SHARED PREF VALUES
    private String key;
    private String uuid;

    //VIEWS
    private TextView fragmentProfileNameTV, fragmentProfileEmployeeIDTV
            , fragmentProfilePositionTV, fragmentProfileDivisionTV
            , fragmentProfileBranchTV, fragmentProfileEmailTV
            , fragmentProfilePhoneTV, fragmentProfileAddressTV;
    private Button fragmentProfileEditButton;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //TAKE KEY
        SharedPreferences prefs = getActivity().getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
        key = prefs.getString("key", "empty");
        Log.d("KEYREIMBURSE", "loadInitial: " + key);
        //TAKE OTHER VALUES
        uuid = prefs.getString("useruuid", "empty");

        //FINDVIEWS
        fragmentProfileAddressTV = view.findViewById(R.id.fragment_profile_address_tv);
        fragmentProfileBranchTV = view.findViewById(R.id.fragment_profile_branch_tv);
        fragmentProfileDivisionTV = view.findViewById(R.id.fragment_profile_division_tv);
        fragmentProfileEmailTV = view.findViewById(R.id.fragment_profile_email_tv);
        fragmentProfileEmployeeIDTV = view.findViewById(R.id.fragment_profile_employeeid_tv);
        fragmentProfileNameTV = view.findViewById(R.id.fragment_profile_name_tv);
        fragmentProfilePhoneTV = view.findViewById(R.id.fragment_profile_phone_tv);
        fragmentProfilePositionTV = view.findViewById(R.id.fragment_profile_positition_tv);
        fragmentProfileEditButton = view.findViewById(R.id.fragment_profile_edit_button);

        enqueUserData();

        //ONCLICK
        fragmentProfileEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    private void enqueUserData () {

        ProgressDialog progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
        progressDoalog.setCanceledOnTouchOutside(false);

        RetrofitClientInstance
                .getRetrofitInstance()
                .create(ApiService.class)
                .getKaryawanDetail("Bearer " + key , uuid)
                .enqueue(new Callback<KaryawanServerResponse>() {
                    @Override
                    public void onResponse(Call<KaryawanServerResponse> call, Response<KaryawanServerResponse> response) {
                        Log.d("KARYAWANDETAIL", "onResponse: " + response.raw().toString());

                        if (response.isSuccessful()) {
                            progressDoalog.dismiss();

                            if (response.body() != null) {

                                try {
                                    Log.d("KARYAWANDETAIL", "onResponse: GET DATA : "+
                                            response.body().getOutput_schema().getUser().getName());

                                    //SETUP TV
                                    fragmentProfileAddressTV.setText(response.body().getOutput_schema().getUser().getAddress());
                                    fragmentProfileBranchTV.setText(response.body().getOutput_schema().getUser().getBranch());
                                    fragmentProfileDivisionTV.setText(response.body().getOutput_schema().getUser().getDivision());
                                    fragmentProfileEmailTV.setText(response.body().getOutput_schema().getUser().getEmail());
                                    fragmentProfileEmployeeIDTV.setText(response.body().getOutput_schema().getUser().getEmployeeID());
                                    fragmentProfileNameTV.setText(response.body().getOutput_schema().getUser().getName());
                                    fragmentProfilePhoneTV.setText(response.body().getOutput_schema().getUser().getPhone());
                                    fragmentProfilePositionTV.setText(response.body().getOutput_schema().getUser().getRole());

                                    SharedPreferences sharedPref = getActivity().getSharedPreferences("CORONA_PROJECT_MANTAP", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPref.edit();

                                    editor.putString("useraddress", response.body().getOutput_schema().getUser().getAddress());
                                    editor.putString("userphone", response.body().getOutput_schema().getUser().getPhone());
                                    editor.putString("useremail", response.body().getOutput_schema().getUser().getEmail());
                                    editor.commit();

                                }catch (Exception e) {
                                    Log.d("KARYAWANDETAIL", "onResponse: DATA FAILURE" +
                                            e);
                                }
                            }
                        } else {
                            progressDoalog.dismiss();
                            Toast.makeText(getActivity(), "API Error! Please try again"
                                    , Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<KaryawanServerResponse> call, Throwable t) {
                        Log.d("KARYAWANDETAILFAIL", "onFailure: " + t);

                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        enqueUserData();
    }
}