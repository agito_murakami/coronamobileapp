package com.example.projectmantap11.application;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import java.util.Timer;
import java.util.TimerTask;

public class CoronaProjectMantap extends Application implements LifecycleObserver {

    private boolean reset = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.d("APPINSTANCE", "App in background");
        reset = false;


        Timer timerObj = new Timer();
        TimerTask timerTaskObj = new TimerTask() {
            public void run() {
                //perform your action here
                Log.d("TIMERINSTANCE", "run: TIME'S UP" );
                reset = true;
                timerObj.cancel();
                timerObj.purge();
            }
        };
        timerObj.schedule(timerTaskObj, 30000, 1000);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.d("APPINSTANCE", "App in foreground");
        if (reset) {
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage(getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            reset = false;
        }
    }
}